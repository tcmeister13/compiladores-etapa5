/* A Bison parser, made by GNU Bison 2.5.  */

/* Bison interface for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2011 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     TK_OC_OR = 259,
     TK_OC_AND = 260,
     TK_OC_NE = 261,
     TK_OC_EQ = 262,
     TK_OC_GE = 263,
     TK_OC_LE = 264,
     UMINUS = 265,
     TK_PR_THEN = 266,
     TK_PR_ELSE = 267,
     TK_PR_INT = 268,
     TK_PR_FLOAT = 269,
     TK_PR_BOOL = 270,
     TK_PR_CHAR = 271,
     TK_PR_STRING = 272,
     TK_PR_IF = 273,
     TK_PR_WHILE = 274,
     TK_PR_DO = 275,
     TK_PR_INPUT = 276,
     TK_PR_OUTPUT = 277,
     TK_PR_RETURN = 278,
     TK_LIT_INT = 279,
     TK_LIT_FLOAT = 280,
     TK_LIT_FALSE = 281,
     TK_LIT_TRUE = 282,
     TK_LIT_CHAR = 283,
     TK_LIT_STRING = 284,
     TK_IDENTIFICADOR = 285,
     TOKEN_ERRO = 286
   };
#endif



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 2068 of yacc.c  */
#line 34 "parser.y"

	struct comp_tree_t* syntax_no;
	struct comp_dict_t* escopo_dic;
	int tipo_var;
	int valor_int;
	int tipo_op;
	struct tipo_vetor* vetorMulti;



/* Line 2068 of yacc.c  */
#line 91 "/home/tatiana/compiladores-etapa5/build/parser.h"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;


