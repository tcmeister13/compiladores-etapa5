/*
 * Tatiana Costa Meister - 205691
 * Clei Antonio de Souza Junior - 207298
 * Jayne Guerra Ceconello -  205678
*/

%{

#include <stdio.h>
#include <string.h>
#include "main.h"

extern int linha;
extern char* yytext;
extern comp_tree_t* arvore;
extern escopo_tree_t* tabela_simbolos_escopo;

int retorno_parser;
int decl_id = 1; //quando 1 ids são inclusos na tabela de simbolos, quando 0 significa acesso, e verifica se está na tabela de simbolos
%}

%left "expr"
%left TK_OC_OR
%left TK_OC_AND
%nonassoc TK_OC_EQ TK_OC_NE
%nonassoc '<' '>' TK_OC_LE TK_OC_GE
%left '!'
%left '+' '-'
%left '*' '/'
%left UMINUS
%right TK_PR_ELSE TK_PR_THEN

%union
{
	struct comp_tree_t* syntax_no;
	struct comp_dict_t* escopo_dic;
	int tipo_var;
	int valor_int;
	int tipo_op;
	struct tipo_vetor* vetorMulti;
}

%token<tipo_var> TK_PR_INT
%token<tipo_var> TK_PR_FLOAT
%token<tipo_var> TK_PR_BOOL
%token<tipo_var> TK_PR_CHAR
%token<tipo_var> TK_PR_STRING
%token TK_PR_IF
%token TK_PR_THEN
%token TK_PR_ELSE
%token TK_PR_WHILE
%token TK_PR_DO
%token TK_PR_INPUT
%token TK_PR_OUTPUT
%token TK_PR_RETURN
%token TK_OC_LE
%token TK_OC_GE
%token TK_OC_EQ
%token TK_OC_NE
%token TK_OC_AND
%token TK_OC_OR
%token TK_LIT_INT
%token TK_LIT_FLOAT
%token TK_LIT_FALSE
%token TK_LIT_TRUE
%token TK_LIT_CHAR
%token TK_LIT_STRING
%token TK_IDENTIFICADOR
%token TOKEN_ERRO


%type<tipo_var> tipo
%type<syntax_no> literal
%type<escopo_dic> decl_s
%type<escopo_dic> decl_v
%type<valor_int> literal_int
%type<escopo_dic> lst_param
%type<escopo_dic> params

%type<syntax_no> id_v
%type<syntax_no> id_d
%type<syntax_no> id_s
%type<syntax_no> expr
%type<syntax_no> acesso_id
%type<syntax_no> s
%type<syntax_no> prog
%type<syntax_no> func
%type<syntax_no> cab
%type<syntax_no> corpo
%type<syntax_no> seq_com
%type<syntax_no> elem_seq_com
%type<syntax_no> com_u
%type<syntax_no> com_s
%type<syntax_no> com_b
%type<syntax_no> atrib
%type<syntax_no> fluxo
%type<syntax_no> input
%type<syntax_no> output
%type<syntax_no> retorno
%type<syntax_no> f_cham
%type<syntax_no> args
%type<syntax_no> lst_arg
%type<syntax_no> lst_saida
%type<syntax_no> elemento


%type<tipo_op> opArit
%type<tipo_op> opLog

%type<vetorMulti> dim_v

%start s

%%

s		: 	  prog 
			  {
				arvore = arvore_pai(IKS_AST_PROGRAMA,(comp_tree_t*)$1);
				return retorno_parser;
			  } ;

prog		:	  decl_global prog 
			  { 
				$$ = $2;				
				retorno_parser = IKS_SYNTAX_SUCESSO;
			  }
			| /* empty*/ 
			  { 
				$$ = NULL;
				retorno_parser = IKS_SYNTAX_SUCESSO;
			  }
			| func prog 
			  { 	
				$$ = (struct comp_tree_t*) arvore_insere_filho((comp_tree_t*)$1,(comp_tree_t*)$2);
				retorno_parser = IKS_SYNTAX_SUCESSO; 
			  }
			| error 
			  { 
				yyerrok; yyclearin; 
				retorno_parser = IKS_SYNTAX_ERRO;
				return IKS_SYNTAX_ERRO;
			  };
		
decl_global	:	  decl_s ';' { tabela_simbolos_escopo = escopoGlobal_Variavel(tabela_simbolos_escopo,(comp_dict_t*) $1);}
			| decl_v ';' { tabela_simbolos_escopo = escopoGlobal_Vetor(tabela_simbolos_escopo,(comp_dict_t*) $1);};

decl_s		:	  tipo  ':' id_s 
			  { 
				$$ = (struct comp_dict_t*) dicionario_declID((comp_tree_t*) $3, $1);
			  };

decl_v		:	  decl_s dim_v
			  { 
				$$ = (struct comp_dict_t*) dicionario_fatorVetor((tipo_vetor*) $2,(comp_dict_t*) $1);
			  };

dim_v		:	  '[' literal_int ']' dim_v { $$ = dicionario_novaDimensao($2,(tipo_vetor*) $4); }
			| '[' literal_int ']' { $$ = dicionario_novaDimensao($2,NULL); } ;

func		:	  cab decl_local {decl_id=0;} corpo 
			  {
				decl_id=1; 
				$$ = (struct comp_tree_t*) arvore_insere_filho((comp_tree_t*)$1,(comp_tree_t*)$4);
				
			  };

cab		:	  decl_s '(' params ')' 
			  { 			
				tabela_simbolos_escopo = escopoGlobal_Funcao(tabela_simbolos_escopo,(comp_dict_t*) $1, (comp_dict_t*) $3);
				$$ = (struct comp_tree_t*) arvore_id_funcao();
			  };

params		:	  lst_param {$$ = $1;}
			| /*empty*/ {$$ = NULL;};

lst_param	:	  decl_s ',' lst_param { $$ = (struct comp_dict_t*) escopo_insereParametro((comp_dict_t*) $1,(comp_dict_t*) $3);}
			| decl_s { $$ =  (struct comp_dict_t*) escopo_criaParametro((comp_dict_t*) $1);};

decl_local	:	  decl_s ';' decl_local { tabela_simbolos_escopo = escopoLocal(tabela_simbolos_escopo, (comp_dict_t*) $1);}
			| /* empty */ ;

corpo		:	  '{' seq_com '}' { $$ = $2; };

seq_com		:	  elem_seq_com ';' seq_com { $$ = (struct comp_tree_t*) arvore_insere_filho((comp_tree_t*) $1,(comp_tree_t*) $3); }
			| elem_seq_com {$$ = $1;} ;

elem_seq_com	:	  com_u {$$ = $1; }
			| /*empty*/ {$$ = NULL;} ;

com_s		:	  atrib {$$=$1;}
			| fluxo {$$=$1;}
			| input {$$=$1;}
			| output {$$=$1;}
			| retorno {$$=$1;}
			| f_cham {$$=$1;};

com_b		:	  '{' seq_com '}' { $$ = (struct comp_tree_t*) arvore_pai(IKS_AST_BLOCO,(comp_tree_t*)$2);};

com_u		:	  com_s {$$=$1;}
			| com_b {$$=$1;};

atrib		:	  acesso_id '=' expr { $$ = (struct comp_tree_t*) arvore_pai(IKS_AST_ATRIBUICAO,(comp_tree_t*)$1,(comp_tree_t*)$3); } ;

fluxo		:	  TK_PR_IF '(' expr ')' TK_PR_THEN com_u { $$ = (struct comp_tree_t*) arvore_pai(IKS_AST_IF_ELSE,(comp_tree_t*)$3,(comp_tree_t*)$6); }
			| TK_PR_IF '(' expr ')' TK_PR_THEN com_u TK_PR_ELSE com_u { $$ = (struct comp_tree_t*) arvore_pai(IKS_AST_IF_ELSE,(comp_tree_t*)$3,(comp_tree_t*)$6,(comp_tree_t*)$8); }
			| TK_PR_WHILE '(' expr ')' TK_PR_DO com_u { $$ = (struct comp_tree_t*) arvore_pai(IKS_AST_WHILE_DO,(comp_tree_t*)$3,(comp_tree_t*)$6); }
			| TK_PR_DO com_u TK_PR_WHILE '(' expr ')' { $$ = (struct comp_tree_t*) arvore_pai(IKS_AST_DO_WHILE,(comp_tree_t*)$2,(comp_tree_t*)$5); };

input		:	  TK_PR_INPUT elemento { $$ = (struct comp_tree_t*) arvore_pai(IKS_AST_INPUT,(comp_tree_t*)$2); };

output		:	  TK_PR_OUTPUT lst_saida { $$ = (struct comp_tree_t*) arvore_pai(IKS_AST_OUTPUT,(comp_tree_t*)$2); };

lst_saida	:	  expr ',' lst_saida { $$ = (struct comp_tree_t*) arvore_insere_filho((comp_tree_t*)$1,(comp_tree_t*)$3);}
			| expr {$$ = $1;} ;

retorno		:	  TK_PR_RETURN expr { $$ = (struct comp_tree_t*) arvore_pai(IKS_AST_RETURN,(comp_tree_t*)$2);};

f_cham		:	  id_s {usoID_Funcao((comp_tree_t*)$1);} '(' args ')' 
			  { $$ = (struct comp_tree_t*) arvore_pai(IKS_AST_CHAMADA_DE_FUNCAO,(comp_tree_t*)$1,(comp_tree_t*)$4);};

args		:	  lst_arg {$$=$1;}
			| /*empty*/ {$$=NULL;};

lst_arg		:	  expr ',' lst_arg { $$ = (struct comp_tree_t*) arvore_insere_filho((comp_tree_t*)$1,(comp_tree_t*)$3);}
			| expr {$$=$1;};

expr		:	  '(' expr ')' {$$=$2;}
			| expr opArit %prec "expr" expr { $$ = (struct comp_tree_t*) arvore_pai($2,(comp_tree_t*)$1,(comp_tree_t*)$3);}
			| expr opLog %prec "expr" expr { $$ = (struct comp_tree_t*) arvore_pai($2,(comp_tree_t*)$1,(comp_tree_t*)$3);}
			| '-' expr %prec UMINUS {$$ = (struct comp_tree_t*) arvore_pai(IKS_AST_ARIM_INVERSAO,(comp_tree_t*)$2);}
			| '!' expr { $$ = (struct comp_tree_t*) arvore_pai(IKS_AST_LOGICO_COMP_NEGACAO,(comp_tree_t*)$2);}
			| elemento { $$=$1; };

opArit		:	  '+' {$$=IKS_AST_ARIM_SOMA;}
			| '-' {$$=IKS_AST_ARIM_SUBTRACAO;}
			| '*' {$$=IKS_AST_ARIM_MULTIPLICACAO;}
			| '/' {$$=IKS_AST_ARIM_DIVISAO;};

opLog		:	  TK_OC_OR {$$=IKS_AST_LOGICO_OU;}
			| TK_OC_AND {$$=IKS_AST_LOGICO_E;}
			| TK_OC_LE {$$=IKS_AST_LOGICO_COMP_LE;}
			| TK_OC_GE {$$=IKS_AST_LOGICO_COMP_GE;}
			| TK_OC_EQ {$$=IKS_AST_LOGICO_COMP_IGUAL;}
			| TK_OC_NE {$$=IKS_AST_LOGICO_COMP_DIF;}
			| '<' {$$=IKS_AST_LOGICO_COMP_L;}
			| '>' {$$=IKS_AST_LOGICO_COMP_G;};

elemento	:	  literal {$$=$1;}
			| acesso_id {$$=$1;}
			| f_cham {$$=$1;};

acesso_id	:	  id_s { usoID_Variavel((comp_tree_t*)$1); $$=$1;}
			| id_v {$$=$1;};

id_v		: 	  id_s { usoID_Vetor((comp_tree_t*)$1);} id_d 
			  {
				$$ = (struct comp_tree_t*) arvore_id_vetor((comp_tree_t*)$1, (comp_tree_t*)$3);
			  };

id_d		:	  '[' expr ']' id_d {  $$ = (struct comp_tree_t*) arvore_insere_filho((comp_tree_t*)$2,(comp_tree_t*)$4);}
			| '[' expr ']' { $$ = $2;};

id_s		:	  TK_IDENTIFICADOR
			  {
				if(decl_id)
				{ $$ = (struct comp_tree_t*) arvore_literal(yytext,linha,IKS_TIPO_ESPECIAL);}
				else
				{ $$ = (struct comp_tree_t*) arvore_id(yytext);}
			  };


literal		:	  TK_LIT_INT { $$ = (struct comp_tree_t*) arvore_literal(yytext,linha,IKS_INT);}
			| TK_LIT_FLOAT { $$ = (struct comp_tree_t*)arvore_literal(yytext,linha,IKS_FLOAT);}
			| TK_LIT_CHAR { $$ = (struct comp_tree_t*) arvore_cria_nodo_literal(yytext,linha,IKS_CHAR);}
			| TK_LIT_TRUE { $$ = (struct comp_tree_t*) arvore_cria_nodo_literal(yytext,linha,IKS_BOOL);}
			| TK_LIT_FALSE { $$ = (struct comp_tree_t*) arvore_cria_nodo_literal(yytext,linha,IKS_BOOL);}
			| TK_LIT_STRING { $$ = (struct comp_tree_t*) arvore_cria_nodo_literal(yytext,linha,IKS_STRING);}; 

literal_int	:	  TK_LIT_INT { $$ = atoi(yytext);};

tipo		:	  TK_PR_INT {$$=IKS_INT;}
			| TK_PR_FLOAT {$$=IKS_FLOAT;}
			| TK_PR_BOOL {$$=IKS_BOOL;}
			| TK_PR_CHAR {$$=IKS_CHAR;}
			| TK_PR_STRING {$$=IKS_STRING;};

