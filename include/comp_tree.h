/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/



typedef struct arvore comp_tree_t;
typedef struct filho TipoFilhos;
typedef struct TIPO_COERCAO Coercao;

typedef struct lista comp_list_t;
typedef struct TIPO_DICIONARIO comp_dict_t;

struct arvore{
    int id;	//identificação do nodo
    int tipo_arvore;    //tipo do nodo arvore IKS_AST_..
    int tipo;
    int tamanho;
    int filhos_proprios;
    Coercao *coercao; /* se null não precisa coercao*/
    comp_dict_t *entrada; //entrada correspondente 
    TipoFilhos  *filhos;  /* encadeia para proximo elemento da lista */
    comp_list_t *TACs;
};

struct filho{
    comp_tree_t  *arv;     /* ponteiro para o nodo da arvore */
    TipoFilhos  *prox;    /* encadeia para proximo elemento da lista */
};

struct TIPO_COERCAO
{
    int coercao; //define se precisa coercao
    int de;
    int para;
};


comp_tree_t* arvore_aloca();
void arvore_Liberar(comp_tree_t* arv);
void arvore_imprime(comp_tree_t* arv);

comp_tree_t* arvore_insere_filho(comp_tree_t* arv, comp_tree_t* filho);
comp_tree_t* arvore_insere_filho_puro(comp_tree_t* arv, comp_tree_t* filho);
comp_tree_t* arvore_coercao_filho(comp_tree_t* pai, int filho_converter, int tipo_valido);
comp_tree_t* arvore_coercao(comp_tree_t *convertido, int t_conversor);
comp_tree_t* arvore_coercao_retorno_funcao(comp_tree_t* pai);
comp_tree_t* arvore_coercao_input(comp_tree_t* pai);
comp_tree_t* arvore_coercao_output(comp_tree_t* pai);
comp_tree_t* confere_passagem_funcao(comp_tree_t* pai);
void declara_gv(comp_tree_t* arv);

comp_tree_t* arvore_cria_nodo(int tipo_arvore, comp_dict_t* entrada, ...);
comp_tree_t* arvore_cria_nodo_literal(char* literal, int linha, int tipo);
comp_dict_t* dicionario_declID(comp_tree_t* arv, int tipo);
comp_tree_t* arvore_cria_nodo_id(char* id);
comp_tree_t* arvore_cria_nodo_id_funcao();
comp_tree_t* arvore_insere_filho_id(comp_tree_t* arv, comp_tree_t* filho, int tipo_id);

comp_tree_t* arvore_verifica_semantica(comp_tree_t* arv);
comp_tree_t* arvore_inferencia_de_tipo(comp_tree_t* arv);
comp_tree_t* arvore_inferencia_de_tipo_unica(comp_tree_t* arv,int filho_herdar);
int inferencia_com_tipo(int tipo, int* filhos, int n);
int inferencia_todos_tipo(int tipo, int* filhos, int n);
int arvore_filhosPai(comp_tree_t* filho);

#define arvore_pai(tipo_arvore, ...) arvore_cria_nodo(tipo_arvore, NULL, ##__VA_ARGS__, NULL)
#define arvore_filho(tipo_arvore, entrada, ...) arvore_cria_nodo(tipo_arvore, entrada, ##__VA_ARGS__, NULL)
#define arvore_literal(literal, linha, tipo) arvore_cria_nodo_literal(literal,linha,tipo)
#define arvore_id(identificador) arvore_cria_nodo_id(identificador)
#define arvore_id_vetor(arv,filho) arvore_insere_filho_id_vetor(arv,filho)
#define arvore_id_funcao() arvore_cria_nodo_id_funcao()
