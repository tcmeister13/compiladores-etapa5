/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>

/*
Especifica��o:
    - Cria��o
    - Remo��o
    - Concaten��o
    - Inser��o
*/

typedef struct lista comp_list_t;
typedef struct arvore comp_tree_t;

typedef struct lista
{
	char* rotulo;
        char* opCode;        /* Tipo da opera��o       */
        char* op1;        /* Primeiro operador      */
        char* op2;        /* Segundo operador       */
        char* alvo1;  /* Resultado da opera��o  */
	char* alvo2;
        comp_list_t* ant;        /* Nodo anterior da lista */
        comp_list_t* prox;       /* Pr�ximo nodo da lista  */
};

comp_list_t* TAC_geraCodigo(comp_tree_t* arvore);
comp_list_t* TAC_geraCodigo_ID(comp_tree_t* arvore);
comp_list_t* TAC_geraCodigo_Literal(comp_tree_t* arvore);
comp_list_t* TAC_geraCodigo_Atribuicao(comp_tree_t* arvore, comp_list_t** filhos);
comp_list_t* TAC_geraCodigo_BinOp(comp_tree_t* arvore, comp_list_t** filhos);
comp_list_t* TAC_geraCodigo_BoolOp(comp_tree_t* arvore, comp_list_t** filhos);
comp_list_t* TAC_geraCodigo_Inversao(comp_tree_t* arvore, comp_list_t** filhos);
comp_list_t* TAC_geraCodigo_Negacao(comp_tree_t* arvore, comp_list_t** filhos);
comp_list_t* TAC_geraCodigo_If(comp_tree_t* arvore, comp_list_t** filhos);
comp_list_t* TAC_geraCodigo_WhileDo(comp_tree_t* arvore, comp_list_t** filhos);
comp_list_t* TAC_geraCodigo_DoWhile(comp_tree_t* arvore, comp_list_t** filhos);
comp_list_t* TAC_geraCodigo_VetorIndexado(comp_tree_t* arvore, comp_list_t** filhos);


int deslocamento_arranjos(comp_tree_t* arvore_indices, int k, comp_dict_t* id_vetor);
comp_list_t* lista_copiaCompleta(comp_list_t* ant, comp_list_t* lista);

comp_list_t* lista_concatenaFilhosSemPrimeiro(int i, comp_list_t** filhos);
comp_list_t* lista_concatenaFilhos(int i, comp_list_t** filhos);
comp_list_t* TAC_curtoCircuito_Rotulo(comp_list_t* saida, char* novot, char* novof);
void lista_Liberar(comp_list_t* l);

#define SEM_OPERADOR ""
#define SEM_ROTULO ""
#define loadI "loadI" //carrega constante no registrador
#define storeAI "storeAI"
#define cstoreAI "cstoreAI"
#define loadAI "loadAI"
#define rarp "rarp"
#define bss "bss"


