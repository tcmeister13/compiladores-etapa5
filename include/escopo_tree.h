/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/

typedef struct TIPO_ARVORE_ESCOPO escopo_tree_t;
typedef struct TIPO_ESCOPOS_LOCAIS escopo_local;


struct TIPO_ARVORE_ESCOPO{
    int base_global;
    comp_dict_t* escopo_global;
    escopo_local* escopos_locais;
};

struct TIPO_ESCOPOS_LOCAIS{
   comp_dict_t* escopo_local;
   escopo_local* prox;
};

void escopo_Liberar(escopo_tree_t* escopo_prog);
escopo_tree_t* escopo_criaEscopoGlobal();
escopo_tree_t* escopo_criaEscopoLocal(escopo_tree_t* escopo_prog);
escopo_tree_t* escopo_insereEscopoGlobal(escopo_tree_t* escopo_prog, comp_dict_t* novo_global, int tipo_id);
escopo_tree_t* escopo_insereEscopoLocal(escopo_tree_t* escopo_prog, comp_dict_t* novo_local, int tipo_id);
comp_dict_t* escopo_insereParametro(comp_dict_t* novo, comp_dict_t* params);
escopo_tree_t* escopo_insereFuncaoEscopoGlobal(escopo_tree_t* escopo_prog, comp_dict_t* novo_global, comp_dict_t* params);
escopo_tree_t* escopo_insereParametrosFuncao(escopo_tree_t* escopo_prog, int tipoparam);
comp_dict_t* escopo_localizarEm_LocalAtual_Global(escopo_tree_t* escopo_prog, char* Chave);
void escopo_usoVariavel(comp_tree_t* arv, int tipo_id);

void escopo_imprime(escopo_tree_t* escopo_prog);
comp_dict_t* escopo_Local_Global(escopo_tree_t* escopo_prog,char* Chave);


#define escopoGlobal_Variavel(escopo_prog, novo_global) escopo_insereEscopoGlobal(escopo_prog, novo_global, TIPO_ID_VARIAVEL)
#define escopoGlobal_Vetor(escopo_prog, novo_global) escopo_insereEscopoGlobal(escopo_prog, novo_global, TIPO_ID_VETOR)
#define escopoGlobal_Funcao(escopo_prog,novo_global,params) escopo_insereFuncaoEscopoGlobal(escopo_prog,novo_global,params)
#define escopo_criaParametro(tipo) escopo_insereParametro(tipo,NULL)
#define escopoLocal(escopo_prog, novo_local) escopo_insereEscopoLocal(escopo_prog,novo_local,TIPO_ID_VARIAVEL)
#define usoID_Funcao(arv) escopo_usoVariavel(arv,TIPO_ID_FUNCAO)
#define usoID_Vetor(arv) escopo_usoVariavel(arv,TIPO_ID_VETOR)
#define usoID_Variavel(arv) escopo_usoVariavel(arv,TIPO_ID_VARIAVEL)
