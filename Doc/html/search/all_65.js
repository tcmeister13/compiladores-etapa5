var searchData=
[
  ['echo',['ECHO',['../lexer_8c.html#aad1dc60a04a1d8cfc8b3ded13601e361',1,'lexer.c']]],
  ['entrada',['entrada',['../structarvore.html#a0a0ae4f7a201fda9d43629af35b493f3',1,'arvore']]],
  ['eob_5fact_5fcontinue_5fscan',['EOB_ACT_CONTINUE_SCAN',['../lexer_8c.html#adf4b0db227e07782e28ade353a7ba7a1',1,'lexer.c']]],
  ['eob_5fact_5fend_5fof_5ffile',['EOB_ACT_END_OF_FILE',['../lexer_8c.html#a7f71d7fa2c403eb4b2f38cb9536f3c63',1,'lexer.c']]],
  ['eob_5fact_5flast_5fmatch',['EOB_ACT_LAST_MATCH',['../lexer_8c.html#ad1a0b5ebcabffe388e9e9ebb2619c1fb',1,'lexer.c']]],
  ['errors_2eh',['errors.h',['../errors_8h.html',1,'']]],
  ['escopo_5fcriaescopoglobal',['escopo_criaEscopoGlobal',['../escopo__tree_8h.html#a4af2bf6a9b204613c042a338649b7a18',1,'escopo_criaEscopoGlobal():&#160;escopo_tree.c'],['../escopo__tree_8c.html#a4af2bf6a9b204613c042a338649b7a18',1,'escopo_criaEscopoGlobal():&#160;escopo_tree.c']]],
  ['escopo_5fcriaescopolocal',['escopo_criaEscopoLocal',['../escopo__tree_8h.html#aaeeda2f5469cb073438be6db631e30bf',1,'escopo_criaEscopoLocal(escopo_tree_t *escopo_prog):&#160;escopo_tree.c'],['../escopo__tree_8c.html#aaeeda2f5469cb073438be6db631e30bf',1,'escopo_criaEscopoLocal(escopo_tree_t *escopo_prog):&#160;escopo_tree.c']]],
  ['escopo_5fcriaparametro',['escopo_criaParametro',['../escopo__tree_8h.html#a88aca2d5485294ead235683752572d14',1,'escopo_tree.h']]],
  ['escopo_5fdeslocamentoglobal',['escopo_deslocamentoGlobal',['../escopo__tree_8c.html#a1a194c1464b73ac4fb223df2f8e1d4ec',1,'escopo_tree.c']]],
  ['escopo_5fdeslocamentolocal',['escopo_deslocamentoLocal',['../escopo__tree_8c.html#acdd151eb3297f77b9033fe18ae7f647d',1,'escopo_tree.c']]],
  ['escopo_5fdic',['escopo_dic',['../union_y_y_s_t_y_p_e.html#a28000722da04f616a2d5914a5e2c1dec',1,'YYSTYPE']]],
  ['escopo_5fglobal',['escopo_global',['../struct_t_i_p_o___a_r_v_o_r_e___e_s_c_o_p_o.html#a0ff0666ed75b430d69c637776fd50771',1,'TIPO_ARVORE_ESCOPO']]],
  ['escopo_5fimprime',['escopo_imprime',['../escopo__tree_8h.html#ac93afe872fe2dbcb37fbe22a38698d89',1,'escopo_imprime(escopo_tree_t *escopo_prog):&#160;escopo_tree.c'],['../escopo__tree_8c.html#ac93afe872fe2dbcb37fbe22a38698d89',1,'escopo_imprime(escopo_tree_t *escopo_prog):&#160;escopo_tree.c']]],
  ['escopo_5finsereescopoglobal',['escopo_insereEscopoGlobal',['../escopo__tree_8h.html#a20def44c53b97c7f19c2244e6e82db5d',1,'escopo_insereEscopoGlobal(escopo_tree_t *escopo_prog, comp_dict_t *novo_global, int tipo_id):&#160;escopo_tree.c'],['../escopo__tree_8c.html#a20def44c53b97c7f19c2244e6e82db5d',1,'escopo_insereEscopoGlobal(escopo_tree_t *escopo_prog, comp_dict_t *novo_global, int tipo_id):&#160;escopo_tree.c']]],
  ['escopo_5finsereescopolocal',['escopo_insereEscopoLocal',['../escopo__tree_8h.html#a1ffb8d11e38bd392450b86b129a41fe0',1,'escopo_insereEscopoLocal(escopo_tree_t *escopo_prog, comp_dict_t *novo_local, int tipo_id):&#160;escopo_tree.c'],['../escopo__tree_8c.html#a1ffb8d11e38bd392450b86b129a41fe0',1,'escopo_insereEscopoLocal(escopo_tree_t *escopo_prog, comp_dict_t *novo_local, int tipo_id):&#160;escopo_tree.c']]],
  ['escopo_5finserefuncaoescopoglobal',['escopo_insereFuncaoEscopoGlobal',['../escopo__tree_8h.html#a33e945d1bc57e231baebc5614b6efc12',1,'escopo_insereFuncaoEscopoGlobal(escopo_tree_t *escopo_prog, comp_dict_t *novo_global, comp_dict_t *params):&#160;escopo_tree.c'],['../escopo__tree_8c.html#a33e945d1bc57e231baebc5614b6efc12',1,'escopo_insereFuncaoEscopoGlobal(escopo_tree_t *escopo_prog, comp_dict_t *novo_global, comp_dict_t *params):&#160;escopo_tree.c']]],
  ['escopo_5finsereparametro',['escopo_insereParametro',['../escopo__tree_8h.html#a1df67184b8499c5cf4ac0adb44708446',1,'escopo_insereParametro(comp_dict_t *novo, comp_dict_t *params):&#160;escopo_tree.c'],['../escopo__tree_8c.html#a1df67184b8499c5cf4ac0adb44708446',1,'escopo_insereParametro(comp_dict_t *novo, comp_dict_t *params):&#160;escopo_tree.c']]],
  ['escopo_5finsereparametrosfuncao',['escopo_insereParametrosFuncao',['../escopo__tree_8h.html#a0e76b66f666604e5b742ca1238d2e623',1,'escopo_insereParametrosFuncao(escopo_tree_t *escopo_prog, int tipoparam):&#160;escopo_tree.c'],['../escopo__tree_8c.html#a0e76b66f666604e5b742ca1238d2e623',1,'escopo_insereParametrosFuncao(escopo_tree_t *escopo_prog, int tipoparam):&#160;escopo_tree.c']]],
  ['escopo_5fliberar',['escopo_Liberar',['../escopo__tree_8h.html#a6bb508a1efff4a749e42042052c85b94',1,'escopo_Liberar(escopo_tree_t *escopo_prog):&#160;escopo_tree.c'],['../escopo__tree_8c.html#a6bb508a1efff4a749e42042052c85b94',1,'escopo_Liberar(escopo_tree_t *escopo_prog):&#160;escopo_tree.c']]],
  ['escopo_5flocal',['escopo_local',['../struct_t_i_p_o___e_s_c_o_p_o_s___l_o_c_a_i_s.html#a50b9d5c5e2196317f614d282bd29ad78',1,'TIPO_ESCOPOS_LOCAIS::escopo_local()'],['../escopo__tree_8h.html#a6253a813289dcda536fc4ca00ceb9963',1,'escopo_local():&#160;escopo_tree.h']]],
  ['escopo_5flocalizarem_5flocalatual_5fglobal',['escopo_localizarEm_LocalAtual_Global',['../escopo__tree_8h.html#a4efed63f07f84881c491e5679a3bd315',1,'escopo_localizarEm_LocalAtual_Global(escopo_tree_t *escopo_prog, char *Chave):&#160;escopo_tree.c'],['../escopo__tree_8c.html#a4efed63f07f84881c491e5679a3bd315',1,'escopo_localizarEm_LocalAtual_Global(escopo_tree_t *escopo_prog, char *Chave):&#160;escopo_tree.c']]],
  ['escopo_5ftree_2ec',['escopo_tree.c',['../escopo__tree_8c.html',1,'']]],
  ['escopo_5ftree_2eh',['escopo_tree.h',['../escopo__tree_8h.html',1,'']]],
  ['escopo_5ftree_5ft',['escopo_tree_t',['../escopo__tree_8h.html#a097176696425caf0f0e1be446a86efad',1,'escopo_tree.h']]],
  ['escopo_5fusovariavel',['escopo_usoVariavel',['../escopo__tree_8h.html#a37b6dda5f264b80f73f9f530700a4ebe',1,'escopo_usoVariavel(comp_tree_t *arv, int tipo_id):&#160;escopo_tree.c'],['../escopo__tree_8c.html#a37b6dda5f264b80f73f9f530700a4ebe',1,'escopo_usoVariavel(comp_tree_t *arv, int tipo_id):&#160;escopo_tree.c']]],
  ['escopoglobal_5ffuncao',['escopoGlobal_Funcao',['../escopo__tree_8h.html#a957a331ed41aa28c24cedd03c45b0e66',1,'escopo_tree.h']]],
  ['escopoglobal_5fvariavel',['escopoGlobal_Variavel',['../escopo__tree_8h.html#a2b11be8f32bee97ca23172eca5b849ed',1,'escopo_tree.h']]],
  ['escopoglobal_5fvetor',['escopoGlobal_Vetor',['../escopo__tree_8h.html#a6d34aa5128c64517a428242c816805e5',1,'escopo_tree.h']]],
  ['escopolocal',['escopoLocal',['../escopo__tree_8h.html#ab7cacb83f1828475c2276a97bc90ffc3',1,'escopo_tree.h']]],
  ['escopos_5flocais',['escopos_locais',['../struct_t_i_p_o___a_r_v_o_r_e___e_s_c_o_p_o.html#a60513add8e6198c6bc1f4c1c0ca8e2ce',1,'TIPO_ARVORE_ESCOPO']]]
];
