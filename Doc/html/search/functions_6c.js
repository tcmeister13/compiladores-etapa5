var searchData=
[
  ['lista_5fconcatena',['lista_concatena',['../comp__list_8c.html#ac9dc254d91e714f323d1e393f8aa7992',1,'comp_list.c']]],
  ['lista_5fconcatenafilhos',['lista_concatenaFilhos',['../comp__list_8h.html#a40890eacb35c4d9e688b9449b461cf46',1,'lista_concatenaFilhos(int i, comp_list_t **filhos):&#160;comp_list.c'],['../comp__list_8c.html#a40890eacb35c4d9e688b9449b461cf46',1,'lista_concatenaFilhos(int i, comp_list_t **filhos):&#160;comp_list.c']]],
  ['lista_5fconcatenafilhossemprimeiro',['lista_concatenaFilhosSemPrimeiro',['../comp__list_8h.html#aa08a040e9a3f2c2e1a50871cb232cee7',1,'lista_concatenaFilhosSemPrimeiro(int i, comp_list_t **filhos):&#160;comp_list.c'],['../comp__list_8c.html#aa08a040e9a3f2c2e1a50871cb232cee7',1,'lista_concatenaFilhosSemPrimeiro(int i, comp_list_t **filhos):&#160;comp_list.c']]],
  ['lista_5fcopiacompleta',['lista_copiaCompleta',['../comp__list_8h.html#a8ceed996f4326e6f42bb992a81b6056e',1,'lista_copiaCompleta(comp_list_t *ant, comp_list_t *lista):&#160;comp_list.c'],['../comp__list_8c.html#a8ceed996f4326e6f42bb992a81b6056e',1,'lista_copiaCompleta(comp_list_t *ant, comp_list_t *lista):&#160;comp_list.c']]],
  ['lista_5fcriatac',['lista_criaTAC',['../comp__list_8c.html#af33a969082f556b27885a590502ae1f9',1,'comp_list.c']]],
  ['lista_5fimprime',['lista_imprime',['../comp__list_8c.html#a1284c1ff11a2098e33b5776c27e33b53',1,'comp_list.c']]],
  ['lista_5fliberar',['lista_Liberar',['../comp__list_8h.html#a12dcac6f3f02ca55279ac3b060a93111',1,'lista_Liberar(comp_list_t *l):&#160;comp_list.c'],['../comp__list_8c.html#a12dcac6f3f02ca55279ac3b060a93111',1,'lista_Liberar(comp_list_t *l):&#160;comp_list.c']]]
];
