/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "iks_ast.h"
#include "comp_dict.h"
#include "define.h"
#include "comp_list.h"
#include "comp_tree.h"
#include "escopo_tree.h"



int id_nodo = 0;
extern escopo_tree_t* tabela_simbolos_escopo;
extern int linha;

/**
 * Aloca uma nova árvore
 * name: arvore_aloca
 * @param
 * @return NULL
 *
 */
comp_tree_t* arvore_aloca(){
    comp_tree_t* nova = (comp_tree_t*) malloc(sizeof(comp_tree_t));    
    return nova;
}

/**
 * Libera memoria de uma arvore
 * name: arvore_Liberar
 * @param arv: arvore a ser liberada
 * @return 
 *
 */
void arvore_Liberar(comp_tree_t* arv)
{
	comp_tree_t* aux = arv;
	
	
	if(aux->coercao!=NULL)
		free(aux->coercao);
	if(aux->entrada!=NULL)
		dicionario_Liberar(aux->entrada);
	if(aux->TACs!=NULL)
		lista_Liberar(aux->TACs);
		
	TipoFilhos* auxtf = aux->filhos;
	
	
	//libera as arvores dos filhos
	while(auxtf!=NULL)
	{
		arvore_Liberar(auxtf->arv);
		auxtf=auxtf->prox;
	}
	
	auxtf = aux->filhos;
	TipoFilhos* liberatf = aux->filhos;
	
	//libera o tipo filhos do nodo pai
	while(auxtf!=NULL)
	{
		auxtf = auxtf->prox;
		free(liberatf);
		liberatf=auxtf;
	}
	
	//libera o nodo pai
	free(aux);
	
}

/**
 * Cria nodo numa arvore e já inclui no grafo gv
 * name: arvore_cria_nodo
 * @param id: valor padrão de identificacao do nodo
 * @param entrada: entrada do dicionario referente ao nodo
 * @param filhos: filhos do nodo a ser criado
 * @param id_nodo: id de identificacao do nodo
 * @return Nodo árvore com filhos
 *
 */
comp_tree_t* arvore_cria_nodo(int tipo_arvore, comp_dict_t* entrada, ...)
{
	
	comp_tree_t* arv = arvore_aloca();
	
	arv->entrada = dicionario_CriaCopiaUnica(entrada);
	
	id_nodo++;
	arv->id = id_nodo;
	arv->tipo_arvore = tipo_arvore;
	arv->tipo = 0;	 //fazer inferencia
	arv->tamanho = 0; //TAMANHO_POR_TIPO(tipo);
	arv->filhos_proprios = FILHOS_AST(tipo_arvore);
	
	
	//ajuste do tamanho para possivel literal string
	/*if(tipo == IKS_STRING)
		if(arv->entrada!=NULL)
			arv->tamanho = strlen(arv->entrada->chave)*arv->tamanho;
		else
			arv->tamanho = 0;*/
	
	//inicialização da coercao
	arv->coercao = (Coercao*) malloc(sizeof(Coercao));
		arv->coercao->coercao = 0;
		arv->coercao->de = -1;
		arv->coercao->para = -1;
		
	arv->filhos = NULL;
	
	arv->TACs = NULL;
	
	//coleta filhos a partir dos parametros
	va_list args;
	va_start (args,entrada); //inicia a partir do ultimo argumento fixo
	
	//insere nodos filhos na arvore atual
	int i=0;
	comp_tree_t* arg;
	do
	{
		arg = va_arg(args, comp_tree_t*); //pega um argumento nodo
		arv = arvore_insere_filho_puro(arv,arg); //insere nodo como filho
		i++;		 
		
	} while(arg!=NULL);
	
	
	va_end(args);//libera argumentos 
	
	//ajuste para if sem else
	if(tipo_arvore==IKS_AST_IF_ELSE)
		arv->filhos_proprios = i-1; //i = numero de filhos + 1
	
	arv = arvore_verifica_semantica(arv);
	
	
	arv->TACs = TAC_geraCodigo(arv);
	return arv;

}

/**
 * Verifica semântica e tipos, fazendo inferência e coerção
 * name: arvore_verifica_semantica
 * @param arv: arvore a ser verificada
 * @return arv com os tipos e se foi necessário coerçao
 *
 */
comp_tree_t* arvore_verifica_semantica(comp_tree_t* arv)
{
	switch(arv->tipo_arvore)
	{
		case IKS_AST_PROGRAMA : { if(arv->filhos!=NULL) arv=arvore_inferencia_de_tipo(arv); return arv;};break;        
		case IKS_AST_FUNCAO  : { return arv;};break;
		case IKS_AST_IF_ELSE  : 
		{ 
			arv=arvore_inferencia_de_tipo(arv); 
			arv=arvore_coercao_filho(arv,0,IKS_BOOL);
			return arv;
		};break;             
		case IKS_AST_DO_WHILE  : 
		{ 
			arv=arvore_inferencia_de_tipo(arv); 
			arv=arvore_coercao_filho(arv,0,IKS_BOOL);
			return arv;
		};break;            
		case IKS_AST_WHILE_DO : 
		{ 
			arv=arvore_inferencia_de_tipo(arv); 
			arv=arvore_coercao_filho(arv,0,IKS_BOOL);
			return arv;
		};break; 
		case IKS_AST_INPUT :  
		{ 
			arv=arvore_inferencia_de_tipo(arv); 
			arv=arvore_coercao_input(arv);
			return arv;
		};break;                  
		case IKS_AST_OUTPUT :  
		{ 
			arv=arvore_inferencia_de_tipo(arv); 
			arv=arvore_coercao_output(arv);
			return arv;
		};break;                
		case IKS_AST_ATRIBUICAO  :  
		{ 
			arv=arvore_inferencia_de_tipo_unica(arv,0);
			arv=arvore_coercao_filho(arv,1,-1);
			return arv;
		};break;              
		case IKS_AST_RETURN    :  
		{ 
			arv=arvore_inferencia_de_tipo(arv);
			arv=arvore_coercao_retorno_funcao(arv);
			return arv;
		};break;                
		case IKS_AST_BLOCO   :  { arv=arvore_inferencia_de_tipo(arv);return arv;};break;                 
		case IKS_AST_IDENTIFICADOR  :  { return arv;};break;     
		case IKS_AST_LITERAL     :  { return arv;};break;           
		case IKS_AST_ARIM_SOMA    :  { arv=arvore_inferencia_de_tipo(arv);return arv;};break;           
		case IKS_AST_ARIM_SUBTRACAO  :  { arv=arvore_inferencia_de_tipo(arv);return arv;};break;        
		case IKS_AST_ARIM_MULTIPLICACAO :  { arv=arvore_inferencia_de_tipo(arv);return arv;};break;     
		case IKS_AST_ARIM_DIVISAO     : { arv=arvore_inferencia_de_tipo(arv);return arv;};break;       
		case IKS_AST_ARIM_INVERSAO    :  { arv=arvore_inferencia_de_tipo(arv);return arv;};break;        // - (operador unário -)
		case IKS_AST_LOGICO_E       :  { arv=arvore_inferencia_de_tipo(arv);return arv;};break;           // &&
		case IKS_AST_LOGICO_OU     :  { arv=arvore_inferencia_de_tipo(arv);return arv;};break;          // ||
		case IKS_AST_LOGICO_COMP_DIF   :  { arv=arvore_inferencia_de_tipo(arv);return arv;};break;       // !=
		case IKS_AST_LOGICO_COMP_IGUAL  :  { arv=arvore_inferencia_de_tipo(arv);return arv;};break;       // ==
		case IKS_AST_LOGICO_COMP_LE    : { arv=arvore_inferencia_de_tipo(arv);return arv;};break;       // <=
		case IKS_AST_LOGICO_COMP_GE   :  { arv=arvore_inferencia_de_tipo(arv);return arv;};break;       // >=
		case IKS_AST_LOGICO_COMP_L      :  { arv=arvore_inferencia_de_tipo(arv);return arv;};break;       // <
		case IKS_AST_LOGICO_COMP_G     :  { arv=arvore_inferencia_de_tipo(arv);return arv;};break;      // >
		case IKS_AST_LOGICO_COMP_NEGACAO  :  { arv=arvore_inferencia_de_tipo(arv);return arv;};break;    // !
		case IKS_AST_VETOR_INDEXADO     :  { arv=arvore_inferencia_de_tipo_unica(arv,0);return arv;};break;     // para var[exp] quando o índice exp é acessado no vetor var
		case IKS_AST_CHAMADA_DE_FUNCAO   :  
		{ 
			arv=arvore_inferencia_de_tipo_unica(arv,0);
			arv=confere_passagem_funcao(arv);
			return arv;
		};break;     
		default: printf("inferencia problem\n"); return arv;
		
	}
	
}


/**
 * Verifica se um tipo específico esta na lista de tipos passada
 * name: inferencia_com_tipo
 * @param filhos: lista de tipos
 * @param tipo: tipo a ser buscado
 * @param n: numero de tipos na lista
 * @return 0 se buscado foi encontrado, 1 se não foi encontrado
 *
 */
int inferencia_com_tipo(int tipo, int* filhos, int n)
{
	int achou=0;
	int i=0;
	while(i!=n)
	{
		if(filhos[i]==tipo)
			achou=1;
		i++;
	}
	
	return achou;
}


/**
 * Verifica se todos filhos tem mesmo tipo
 * name: inferencia_todos_tipo
 * @param tipo: tipo a ser verificado
 * @param filhos: todos os tipos dos filhos
 * @param n: número de filhos
 * @return 1 se tem filho com tipo diferente, 0 caso contrário
 *
 */
int inferencia_todos_tipo(int tipo, int* filhos, int n)
{
	int achou=1;
	int i=0;
	while(i!=n)
	{
		if(filhos[i]!=tipo)
			achou=0;
		i++;
	}
	
	return achou;
}

/**
 * Faz inferência de tipos e atribui tamanhos
 * name: arvore_inferencia_de_tipo
 * @param arv: arvore a ser inferidos os tipos
 * @return arv com os tipos e tamanhos
 *
 */
comp_tree_t* arvore_inferencia_de_tipo(comp_tree_t* arv)
{
	int tipo_filhos [arv->filhos_proprios];
	
	int nfilhos = arv->filhos_proprios;
	
	if(arv->filhos==NULL)
		return NULL;
	else
	{
		if(nfilhos==1)
		{
			arv->tipo = arv->filhos->arv->tipo;
			arv->tamanho = TAMANHO_POR_TIPO(arv->tipo);
			return arv;
		}
		else
		{
			//if(arv->filhos->arv = NULL) return -1;
			TipoFilhos* aux = arv->filhos;
			int i=0;
			while(i!=nfilhos)
			{
				tipo_filhos[i]=aux->arv->tipo;
				aux = aux->prox;
				i++;
			}
			
			int com_char = inferencia_com_tipo(IKS_CHAR,tipo_filhos,nfilhos);
			if(com_char)
				if(inferencia_todos_tipo(IKS_CHAR,tipo_filhos,nfilhos))
				{
					arv->tipo = IKS_CHAR;
					arv->tamanho = TAMANHO_POR_TIPO(arv->tipo);
					return arv;
				}
				else
				{
					escopo_Liberar(tabela_simbolos_escopo);
					arvore_Liberar(arv);
					Imprime_Erro(linha,IKS_ERROR_CHAR_TO_X); 
					exit(IKS_ERROR_CHAR_TO_X);
					
					return NULL;
				}
			if(inferencia_com_tipo(IKS_STRING,tipo_filhos,nfilhos))
				if(inferencia_todos_tipo(IKS_STRING,tipo_filhos,nfilhos))
				{
					arv->tipo = IKS_STRING;
					arv->tamanho = TAMANHO_POR_TIPO(arv->tipo);
					return arv;
				}
				else
				{
					escopo_Liberar(tabela_simbolos_escopo);
					arvore_Liberar(arv);
					Imprime_Erro(linha,IKS_ERROR_STRING_TO_X); 
					exit(IKS_ERROR_STRING_TO_X);
					
					return NULL;
				}
			
			
			if(inferencia_com_tipo(IKS_FLOAT, tipo_filhos, nfilhos)==1)
			{
					arv->tipo = IKS_FLOAT;
					arv->tamanho = TAMANHO_POR_TIPO(arv->tipo);
					return arv;
			}
			if(inferencia_com_tipo(IKS_INT, tipo_filhos, nfilhos)==1)
			{
					arv->tipo = IKS_INT;
					arv->tamanho = TAMANHO_POR_TIPO(arv->tipo);
					return arv;
			}
			if(inferencia_com_tipo(IKS_BOOL, tipo_filhos, nfilhos)==1)
			{
					arv->tipo = IKS_BOOL;
					arv->tamanho = TAMANHO_POR_TIPO(arv->tipo);
					return arv;
			}
		}
			
			
	}	
	
}

/**
 * Determina o tipo de um filho
 * name: arvore_inferencia_de_tipo_unica
 * @param arv: arvore a ser verificada
 * @param filho_herdar: tipo a ser herdado
 * @return arv com o tipo 
 *
 */
comp_tree_t* arvore_inferencia_de_tipo_unica(comp_tree_t* arv,int filho_herdar)
{
	int tipo_filhos [arv->filhos_proprios];
	
	int nfilhos = arv->filhos_proprios;
	if(arv->filhos==NULL)
		return NULL;
	else
	{
			TipoFilhos* aux = arv->filhos;
			int i=0;
			while(i!=nfilhos)
			{
				if(aux!=NULL)
				{
					tipo_filhos[i]=aux->arv->tipo;
					aux = aux->prox;
				}
				i++;
			}
			
			arv->tipo = tipo_filhos[filho_herdar];
			arv->tamanho = TAMANHO_POR_TIPO(arv->tipo);
			return arv;
			
			
	}	
	
}


/**
 * Cria nodo de identificador na arvore
 * name: arvore_cria_nodo_id
 * @param id: id do nodo a ser criado
 * @return arv com o novo nodo inserido
 *
 */
comp_tree_t* arvore_cria_nodo_id(char* id)
{
	//Verifica se foi declarado
	comp_dict_t* entrada = escopo_localizarEm_LocalAtual_Global(tabela_simbolos_escopo,id);
	if(entrada==NULL)
	{
		escopo_Liberar(tabela_simbolos_escopo);
		Imprime_Erro(linha,IKS_ERROR_UNDECLARED); 
		exit(IKS_ERROR_UNDECLARED);
	}
	
	comp_tree_t* arv = arvore_aloca();
	
	arv->entrada = dicionario_CriaCopiaUnica(entrada);
	
	
	id_nodo++;
	arv->id = id_nodo;
	arv->tipo_arvore = IKS_AST_IDENTIFICADOR;
	arv->tipo = entrada->tipo;	 //fazer inferencia
	arv->tamanho = entrada->tamanho; //TAMANHO_POR_TIPO(tipo);
	arv->filhos_proprios = FILHOS_AST(arv->tipo_arvore);
	
	
	//inicialização da coercao
	arv->coercao = (Coercao*) malloc(sizeof(Coercao));
		arv->coercao->coercao = 0;
		arv->coercao->de = -1;
		arv->coercao->para = -1;
		
	arv->filhos = NULL;
	
	arv->TACs = TAC_geraCodigo(arv);
	/*printf("\n########\n");
	printf("CRIOU ID: %s\n",id);
	TAC_imprime(arv->TACs);
	printf("\n########\n");*/
	
	dicionario_Liberar(entrada);
	return arv;

}

/**
 * Cria nodo de literal na arvore
 * name: arvore_cria_nodo_literal
 * @param literal: literal a ser criado 
 * @param linha: linha onde ocorre
 * @param tipo: tipo do literal
 * @return arv com o novo nodo inserido
 *
 */
comp_tree_t* arvore_cria_nodo_literal(char* literal, int linha, int tipo)
{

	comp_tree_t* arv = arvore_aloca();

	if(tipo == IKS_CHAR || tipo == IKS_STRING)
	{
		char* sem_aspas = string_copia_sem_primeiro_e_ultimo(literal);
		arv->entrada = dicionario_AdicionaEntrada(sem_aspas,linha,tipo,NULL);
		free(sem_aspas);
	}
	else
		arv->entrada = dicionario_AdicionaEntrada(literal,linha,tipo,NULL);
		
	id_nodo++;
	arv->id = id_nodo;
	arv->tipo_arvore = IKS_AST_LITERAL;
	arv->tipo = tipo;
		
	arv->tamanho = TAMANHO_POR_TIPO(tipo);
	arv->filhos_proprios = 0;
	
	//ajuste do tamanho para possivel literal string
	if(tipo == IKS_STRING)
		if(arv->entrada!=NULL)
			arv->tamanho = strlen(arv->entrada->chave)*arv->tamanho;
		else
			arv->tamanho = 0;
	
	//inicialização da coercao
	arv->coercao = (Coercao*) malloc(sizeof(Coercao));
		arv->coercao->coercao = 0;
		arv->coercao->de = -1;
		arv->coercao->para = -1;
		
	arv->filhos = NULL;
	
	
	arv->TACs = TAC_geraCodigo(arv);
	/*printf("\n########\n");
	printf("CRIOU LITERAL: %s\n",literal);
	TAC_imprime(arv->TACs);
	printf("\n########\n");*/
	return arv;

}

/**
 * Cria nodo de funcao na arvore
 * name: arvore_cria_nodo_id_funcao
 * @return arv com o novo nodo inserido
 *
 */
comp_tree_t* arvore_cria_nodo_id_funcao()
{
	if(tabela_simbolos_escopo!=NULL)
	{
		//dicionario_imprime(tabela_simbolos_escopo->escopo_global);
		comp_dict_t* entrada = tabela_simbolos_escopo->escopo_global;
	
		comp_tree_t* arv = arvore_aloca();
		
		arv->entrada = dicionario_CriaCopiaUnica(entrada);
		
		id_nodo++;
		arv->id = id_nodo;
		arv->tipo_arvore = IKS_AST_FUNCAO;
		arv->tipo = entrada->tipo;	 //fazer inferencia
		arv->tamanho = entrada->tamanho; //TAMANHO_POR_TIPO(tipo);
		arv->filhos_proprios = FILHOS_AST(arv->tipo_arvore);
		
		
		//inicialização da coercao
		arv->coercao = (Coercao*) malloc(sizeof(Coercao));
			arv->coercao->coercao = 0;
			arv->coercao->de = -1;
			arv->coercao->para = -1;
			
		arv->filhos = NULL;
		arv->TACs = TAC_geraCodigo(arv);

		return arv;
	}
	else
		return NULL;

}

/**
 * Cria nodo gv e faz ligaçoes
 * name: declara_gv
 * @param arv: arvore a ser declarada em gv
 *
 */
void declara_gv(comp_tree_t* arv)
{
	if(arv!=NULL)
	{
		if(arv->entrada!=NULL)
			gv_declare(arv->tipo_arvore,arv, arv->entrada->chave);
		else
			gv_declare(arv->tipo_arvore,arv,NULL);

		//conecta todos os filhos ao pai
		TipoFilhos* aux = arv->filhos;
		while(aux!=NULL)
		{
			declara_gv(aux->arv);
			gv_connect(arv,aux->arv);
			aux = aux->prox;
		}
	}
}


/**
 * Insere filho na lista de filhos de um nodo já criado
 * name: arvore_insere_filho
 * @param arv: nodo a ter filho incluido
 * @param novo_filho: novo filho a ser incluido
 * @return Nodo árvore com mais um filho
 *
 */
comp_tree_t* arvore_insere_filho(comp_tree_t* arv, comp_tree_t* filho)
{

	TipoFilhos* aux;
	TipoFilhos *novo_filho = NULL;

	if(filho!=NULL){
		novo_filho= (TipoFilhos*) malloc(sizeof(TipoFilhos));
		novo_filho->arv = filho;
		novo_filho->prox = NULL;
	}
	
	
	
	if(arv->filhos!=NULL)
	{
		aux = arv->filhos;
		while(aux->prox!=NULL)
			aux= aux->prox;
		aux->prox = novo_filho;
	}
	else
	{
		arv->filhos = novo_filho;
	}
	
	if(filho!=NULL)
	{
			/*printf("\n########\n");
			printf("PAI: %s\n",IKS_AST_para_texto(arv->tipo_arvore));
			TAC_imprime(arv->TACs);
			printf("\nFILHO: %s\n",IKS_AST_para_texto(filho->tipo_arvore));
			TAC_imprime(filho->TACs);
			printf("\n");*/
		
			arv->TACs = lista_concatena(filho->TACs,arv->TACs);
			
			/*printf("NOVO PAI: \n");
			TAC_imprime(arv->TACs);
			printf("\n########\n");*/
			
	}
	
	return arv;
	
}


/**
 * Insere filho na lista de filhos de um nodo já criado
 * name: arvore_insere_filho
 * @param arv: nodo a ter filho incluido
 * @param novo_filho: novo filho a ser incluido
 * @return Nodo árvore com mais um filho
 *
 */
comp_tree_t* arvore_insere_filho_puro(comp_tree_t* arv, comp_tree_t* filho)
{

	TipoFilhos* aux;
	TipoFilhos *novo_filho = NULL;

	if(filho!=NULL){
		novo_filho= (TipoFilhos*) malloc(sizeof(TipoFilhos));
		novo_filho->arv = filho;
		novo_filho->prox = NULL;
	}
	
	
	
	if(arv->filhos!=NULL)
	{
		aux = arv->filhos;
		while(aux->prox!=NULL)
			aux= aux->prox;
		aux->prox = novo_filho;
	}
	else
	{
		arv->filhos = novo_filho;
	}
	
	return arv;
	
}

/**
 * Insere filho na lista de filhos de um nodo já criado
 * name: arvore_insere_filho
 * @param arv: nodo a ter filho incluido
 * @param novo_filho: novo filho a ser incluido
 * @return Nodo árvore com mais um filho
 *
 */
comp_tree_t* arvore_insere_filho_id_vetor(comp_tree_t* arv, comp_tree_t* filho)
{

	int tipo_convertido = filho->tipo;
	filho = arvore_coercao(filho,IKS_INT);
	
	if(filho!=NULL)
	{
		if(arv->entrada->tipo_id==TIPO_ID_VETOR)
		{
			int filhos = arvore_filhosPai(filho);
			if(filhos<arv->entrada->vetor->dimensao)
			{
				arvore_Liberar(arv);
				arvore_Liberar(filho);
				escopo_Liberar(tabela_simbolos_escopo);
				Imprime_Erro(linha,IKS_ERROR_VETOR_MISSING); 
				exit(IKS_ERROR_VETOR_MISSING);
			}
			else if(filhos>arv->entrada->vetor->dimensao)
			{
				arvore_Liberar(arv);
				arvore_Liberar(filho);
				escopo_Liberar(tabela_simbolos_escopo);
				Imprime_Erro(linha,IKS_ERROR_VETOR_EXCESS); 
				exit(IKS_ERROR_VETOR_EXCESS);
			}
			comp_tree_t* pai = arvore_cria_nodo(IKS_AST_VETOR_INDEXADO,NULL,arv,filho,NULL);
			
			return pai;
			
		}
		else
			if(arv->entrada->tipo_id==TIPO_ID_VARIAVEL)
			{
				arvore_Liberar(arv);
				arvore_Liberar(filho);
				escopo_Liberar(tabela_simbolos_escopo);
				Imprime_Erro(linha,IKS_ERROR_VARIABLE); 
				exit(IKS_ERROR_VARIABLE);
			}
			if(arv->entrada->tipo_id==TIPO_ID_FUNCAO)
			{
				arvore_Liberar(arv);
				arvore_Liberar(filho);
				escopo_Liberar(tabela_simbolos_escopo);
				Imprime_Erro(linha,IKS_ERROR_FUNCTION); 
				exit(IKS_ERROR_FUNCTION);
			}
	}
	else
	{
		escopo_Liberar(tabela_simbolos_escopo);
		arvore_Liberar(arv);
		Imprime_Erro(linha,tipo_coercao_incompativel(tipo_convertido)); 
		exit(tipo_coercao_incompativel(tipo_convertido));
	}
	
	

	
}


/**
 * Faz coerçao em um filho
 * name: arvore_coercao_filho
 * @param pai: pai do nodo a ser coergido
 * @param filho_converter: número do filho alvo
 * @param tipo_valido: se tipo pode ser coergido
 * @return arv com o nodo coergido
 *
 */
comp_tree_t* arvore_coercao_filho(comp_tree_t* pai, int filho_converter, int tipo_valido)
{
	if(pai!=NULL)
	{
		int t_conversor;
		if(tipo_valido==-1)
			t_conversor = pai->tipo;
		else
			t_conversor = tipo_valido;
		int i=0;
		
		//procura filho de posição filho_converter
		TipoFilhos* filho = pai->filhos;
		while(filho!=NULL && i!=filho_converter)
		{
			i++;
			filho=filho->prox;
			
		}
		
		if(filho!=NULL)
		{
			
			comp_tree_t* convertido = filho->arv;
			int tipo_convertido = convertido->tipo;
			if(convertido->tipo == t_conversor)
				return pai;
			if(arvore_verifica_coercao(convertido->tipo,t_conversor)==1)
			{
				convertido->coercao->coercao=1;
				convertido->coercao->de = convertido->tipo;
				convertido->coercao->para = t_conversor;
				return pai;
			}
			else 
			{
				escopo_Liberar(tabela_simbolos_escopo);
				arvore_Liberar(pai);
				Imprime_Erro(linha,tipo_coercao_incompativel(tipo_convertido)); 
				exit(tipo_coercao_incompativel(tipo_convertido));
				return NULL;
			}
		}
		else
			return NULL;
	}
	else
		return NULL;
}

/**
 * Faz coerção no retorno de uma funçao
 * name: arvore_coerçao_retorno_funcao
 * @param pai: pai do nodo a ser coergido
 * @return arv com o nodo coergido
 *
 */
comp_tree_t* arvore_coercao_retorno_funcao(comp_tree_t* pai)
{
	if(pai!=NULL)
	{
		int t_conversor = tabela_simbolos_escopo->escopo_global->funcao->tipo_retorno;
		
		comp_tree_t* convertido = pai->filhos->arv;
		convertido = arvore_coercao(convertido,t_conversor);
		
		if(convertido!=NULL)
			return pai;
		else 
		{
			escopo_Liberar(tabela_simbolos_escopo);
			//arvore_Liberar(pai);
			Imprime_Erro(linha,IKS_ERROR_WRONG_PAR_RETURN); 
			exit(IKS_ERROR_WRONG_PAR_RETURN);
			return NULL;
		}
	}
	else
		return NULL;
}

/**
 * Faz coerção no input
 * name: arvore_coerçao_input
 * @param pai: pai do nodo a ser coergido
 * @return arv com o nodo coergido
 *
 */
comp_tree_t* arvore_coercao_input(comp_tree_t* pai)
{
	if(pai!=NULL)
	{
		
		
		comp_tree_t* convertido = pai->filhos->arv;
		
		if(convertido->entrada->tipo_id!=-1)
			return pai;
		else 
		{
			escopo_Liberar(tabela_simbolos_escopo);
			arvore_Liberar(pai);
			Imprime_Erro(linha,IKS_ERROR_WRONG_PAR_INPUT); 
			exit(IKS_ERROR_WRONG_PAR_INPUT);
			return NULL;
		}
	}
	else
		return NULL;
}

/**
 * Faz coerção no output
 * name: arvore_coerçao_output
 * @param pai: pai do nodo a ser coergido
 * @return arv com o nodo coergido
 *
 */
comp_tree_t* arvore_coercao_output(comp_tree_t* pai)
{
	if(pai!=NULL)
	{
		
		
		comp_tree_t* convertido = pai->filhos->arv;
		
		if(convertido->tipo!=IKS_CHAR)
			return pai;
		else 
		{
			escopo_Liberar(tabela_simbolos_escopo);
			arvore_Liberar(pai);
			Imprime_Erro(linha,IKS_ERROR_WRONG_PAR_OUTPUT); 
			exit(IKS_ERROR_WRONG_PAR_OUTPUT);
			return NULL;
		}
	}
	else
		return NULL;
}

/**
 * Verifica se parametros passados a uma chamada de função estão corretos, devolve tipo de erro ou 0 se estiver certo
 * name: confere_passagem_funcao
 * @param Dicionario: onde buscar funcao
 * @param Chave: funcao a ser verificada
 * @param f_cham: arvore da chamada de funcao
 * @return erro que ocorreu ou sucesso (0)
 *
 */
comp_tree_t* confere_passagem_funcao(comp_tree_t* pai)
{
     if(pai!=NULL)
     {
		 comp_dict_t* Dicionario = tabela_simbolos_escopo->escopo_global;
		 char* Chave = pai->filhos->arv->entrada->chave;
		 int i=0;
		 TipoFilhos* filhos = pai->filhos;
		 while(i!=1 && filhos!=NULL)
		 {
			 i++;
			 filhos=filhos->prox;
		 }
		 comp_tree_t* f_cham;
		 
		 if(filhos!=NULL)
			f_cham = filhos->arv;
		 else
			f_cham = NULL;
		 comp_dict_t *ant = NULL; //ponteiro auxiliar para a posição anterior
		 comp_dict_t *ptaux = Dicionario; //ponteiro auxiliar para percorrer a lista
		//printf("conferindo %s funcao\n",Chave);
		 /*procura o elemento na lista*/
		 while (ptaux !=NULL && (strcmp(ptaux->chave, Chave)!=0))
		 {
			  ant = ptaux;
			  ptaux = ptaux->prox;
		 }
		//printf("$$\n");arvore_imprime_visual(f_cham);
		 /*verifica se achou*/
		 if (ptaux == NULL || strcmp(ptaux->chave,Chave)!=0)
		  {
			  
			  return pai; /*retorna a lista original*/
		  }
		 else 
		 {
			//confere parametros
			TipoFilhos* filhos_fn = NULL;
			TipoFilhos* filhos_f = NULL;
			if(f_cham!=NULL) 
			{ 	filhos_fn = (TipoFilhos*) malloc(sizeof(TipoFilhos*)); 
				filhos_fn->arv = f_cham; 
				filhos_f=filhos_fn;
			}
			int igual=1;
			
			tipo_param* param_f = ptaux->funcao->param;
			
				
			while(param_f!=NULL && igual == 1 && filhos_f!=NULL)
			{
				//printf("Param_f->tipo %s\nfilhos_f->arv->tipo %s\n\n", IKS_TIPO_para_texto(param_f->tipo), IKS_AST_para_texto(filhos_f->arv->nodo));			
				
				comp_tree_t* arvC = filhos_f->arv;
				int tipo_convertido = arvC->tipo;
				arvC = arvore_coercao(arvC,param_f->tipo);
				if(arvC==NULL) {igual=0;}
				else
				{
					int i = arvC->filhos_proprios -1;
					TipoFilhos* f = filhos_f->arv->filhos;
					while(i>=0)
					{ if(f!=NULL) f=f->prox; i--;}
					filhos_f=f;
				
					param_f = param_f->prox;
				}
			}
			
			
			if(filhos_f==NULL && param_f!=NULL)
			{
				escopo_Liberar(tabela_simbolos_escopo);
				
				Imprime_Erro(linha,IKS_ERROR_MISSING_ARGS); 
				exit(IKS_ERROR_MISSING_ARGS);
				return NULL;//faltam argumentos
			}
			if(filhos_f!=NULL && param_f==NULL)
			{
				escopo_Liberar(tabela_simbolos_escopo);
				free(filhos_fn);
				Imprime_Erro(linha,IKS_ERROR_EXCESS_ARGS); 
				exit(IKS_ERROR_EXCESS_ARGS);
				return NULL;//faltam argumentos
			}
			if(igual!=1)
			{
				escopo_Liberar(tabela_simbolos_escopo);
				
				Imprime_Erro(linha,IKS_ERROR_WRONG_TYPE_ARGS); 
				exit(IKS_ERROR_WRONG_TYPE_ARGS);
				return NULL;//faltam argumentos
			}		
			
			//arvore_Liberar(filhos_f->arv);
			free(filhos_fn);
			return pai;
		}
				
     }
     else
			return NULL;
}

/**
 * Retorna nodo atualizado com campo coerção
 * name: arvore_coercao
 * @param convertido: nodo que poderá ser convertido
 * @param t_conversor: tipo do conversor
 * @return nodo convertido atualizado
 *
 */
comp_tree_t* arvore_coercao(comp_tree_t *convertido, int t_conversor)
{
	if(convertido->tipo == t_conversor)
		return convertido;
	if(arvore_verifica_coercao(convertido->tipo,t_conversor)==1)
	{
		convertido->coercao->coercao=1;
		convertido->coercao->de = convertido->tipo;
		convertido->coercao->para = t_conversor;
		return convertido;
	}
	else 
	{
		arvore_Liberar(convertido);
		return NULL;
	}
}

/**
 * Retorna se é possivel coerção (1) ou não (0)
 * name: arvore_verifica_coercao
 * @param t_convertido: tipo do nodo que quer ser convertido
 * @param t_conversor: tipo do conversor
 * @return se é possível coerção
 *
 */
int arvore_verifica_coercao(int t_convertido, int t_conversor)
{
	if(t_convertido == IKS_INT)
	{
		if(t_conversor == IKS_FLOAT || t_conversor == IKS_BOOL)
			return 1;
		else
			return 0;
	}
	if(t_convertido == IKS_BOOL)
	{
		if(t_conversor == IKS_FLOAT || t_conversor == IKS_INT)
			return 1;
		else
			return 0;
	}
	if(t_convertido == IKS_FLOAT)
	{
		if(t_conversor == IKS_INT || t_conversor == IKS_BOOL)
			return 1;
		else
			return 0;
	}
	
	return 0;
}

/**
 * Inserçao de um novo id declarado
 * name: dicionario_declID
 * @param arv: arvore com o id a ser declarado
 * @param tipo: tipo do id declarado
 * @return novo nodo declarado
 *
 */
comp_dict_t* dicionario_declID(comp_tree_t* arv, int tipo)
{
	comp_dict_t* novo;
	
	novo = dicionario_Alocar();
	
	novo->linha = arv->entrada->linha;
	novo->tipo = tipo;
	novo->tipo_id = -1;
	novo->funcao = NULL;
	novo->vetor = NULL;
	novo->prox = NULL;	
	novo->chave = string_copia(arv->entrada->chave);
	
	//tamanho
	novo->tamanho = TAMANHO_POR_TIPO(tipo);
	
	arvore_Liberar(arv);
	
	return novo;
}

/**
 * Retorna contagem de filhos
 * name: arvore_filhosPai
 * @param filho: nodo a serem contados filhos
 * @return numero de filhos
 *
 */
int arvore_filhosPai(comp_tree_t* filho)
{
	comp_tree_t* aux = filho;
	int filhos = 0;
	
	if(aux!=NULL)
	{
		filhos++;
		int filhos_proprio = filho->filhos_proprios;
		
		TipoFilhos* tfaux=filho->filhos;
		int i=0;
		while(i<filhos_proprio)
		{
			if(tfaux!=NULL)
				tfaux=tfaux->prox;
			i++;
		}
		
		if(tfaux!=NULL)
		{
			filhos+=arvore_filhosPai(tfaux->arv);
			return filhos;
		}
		else
			return filhos;
	}
	return 0;
		
}


/**
 * Imprime arvore (printf) na tela
 * name: arvore_imprime
 * @param arv: arvore a ser impressa
 * @return
 *
 */
void arvore_imprime(comp_tree_t* arv)
{
    if(arv!=NULL)
    {
		TipoFilhos* filhos = arv->filhos;
		
		printf("\nID: %d\n", arv->id);
		printf("Tipo arvore: %s\n", (char*) IKS_AST_para_texto(arv->tipo_arvore));
		printf("Tipo: %s\n", (char*) IKS_TIPO_para_texto(arv->tipo));
		printf("Tamanho: %d\n", arv->tamanho);
		printf("Filhos proprios: %d\n", arv->filhos_proprios);
		
		//coercao
		if(arv->coercao->coercao==1)
			printf("Precisa de coercao de %s para %s\n", IKS_TIPO_para_texto(arv->coercao->de), IKS_TIPO_para_texto(arv->coercao->para));
		else
			printf("Nao precisa de coercao\n");
		
		//entrada do dicionario
		if(arv->entrada!=NULL)
			dicionario_imprime(arv->entrada);
		else
			printf("Sem entrada no dicionario\n");
		
		printf("Filhos ID: ");
		while(filhos!=NULL)
		{
			printf("%d ",filhos->arv->id);
			filhos = filhos->prox;
		}
		printf("\n");
		
		printf("\nTACs:\n");
		TAC_imprime(arv->TACs);
		//chama recursivamente para filhos
		filhos = arv->filhos;
		
		printf("\n#############################\n");
		while(filhos!=NULL)
		{
			arvore_imprime(filhos->arv);
			filhos = filhos->prox;
		}
		
		
		return;
	}
	else
		printf("ARVORE VAZIA\n");
	
}



