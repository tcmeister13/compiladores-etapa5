/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stringf.h"
#include "define.h"
#include "comp_tree.h"
#include "comp_dict.h"
#include "comp_list.h"
#include "escopo_tree.h"

extern escopo_tree_t* tabela_simbolos_escopo;
#define MAX_FILHOS 4


/**
 * Realiza a libera�ao da memoria de uma lista
 * name: lista_Liberar
 * @param l: lista que ser� liberada
 *
 */
void lista_Liberar(comp_list_t* l)
{
	comp_list_t* aux = l;
	comp_list_t* liberar = l;
	
	while(aux!=NULL)
	{
		aux = aux->prox;
		free(liberar->op1);
		free(liberar->op2);
		free(liberar->alvo1);
		free(liberar->alvo2);
		free(liberar->opCode);
		free(liberar->rotulo);
		free(liberar);
		liberar=aux;
	}
}

/**
 * Cria um elemento da lista com os dados
 * name: lista_cria_info
 * @param tipo: tipo da operacao
 * @param op1: primeiro operador
 * @param op2: segundo operador
 * @param resultado: resultado da operacao
 * @return Novo TipoInfo_l
 *
 */
comp_list_t* lista_criaTAC(char* rotulo, char* tipo, char *op1, char *op2, char *alvo1, char *alvo2){
	comp_list_t *novo;
    novo = (comp_list_t*) malloc(sizeof(comp_list_t));

	novo->rotulo = string_copia(rotulo);
    novo->opCode = string_copia(tipo);
    novo->op1 = string_copia(op1);
    novo->op2 = string_copia(op2);
    novo->alvo1 = string_copia(alvo1);
    novo->alvo2 = string_copia(alvo2);
    novo->ant=NULL;
    novo->prox=NULL;

    return novo;
}

/**
 * Concatena duas listas, lista 2 ao fim da lista 1
 * name: lista_concatena
 * @param l1: primeira lista
 * @param l2: segunda lista
 * @return Lista contendo l1 e l2 concatenadas
 *
 */
comp_list_t* lista_concatena(comp_list_t* l1, comp_list_t* l2){

    if(l1!=NULL)
    {
		comp_list_t *ptaux = l1;
		while(ptaux->prox != NULL){
			ptaux = ptaux->prox;
		}
		/* Achou posi��o */
		ptaux->prox = l2;

		if(l2!=NULL)
			l2->ant = ptaux;

		return l1;
	}
	else
		return l2;
}

/**
 * Concatena todos os filhos
 * name: lista_concatenaFilhos
 * @param filhos: filhos que ser�o concatenados
 * @param i: numero de filhos
 *
 */
comp_list_t* lista_concatenaFilhos(int i, comp_list_t** filhos)
{
	//come�a com MAX_FILHOS
	i--;
	if(i<0)
		return NULL;
	else
		return lista_concatena(filhos[i],lista_concatenaFilhos(i,filhos));

}

/**
 * Concatena todos os filhos, exceto o primeiro (atribuicao sem o load)
 * name: lista_concatenaFilhos
 * @param filhos: filhos que ser�o concatenados
 * @param i: numero de filhos
 *
 */
comp_list_t* lista_concatenaFilhosSemPrimeiro(int i, comp_list_t** filhos)
{
	//come�a com MAX_FILHOS
	i--;
	if(i<1)
		return NULL;
	else
		return lista_concatena(filhos[i],lista_concatenaFilhosSemPrimeiro(i,filhos));

}

/**
 * Copia uma lista
 * name: lista_copiaCompleta
 * @param ant: lista anterior
 * @param lista: lista
 *
 */
comp_list_t* lista_copiaCompleta(comp_list_t* ant, comp_list_t* lista)
{
	comp_list_t* ptaux = lista;
	if(lista!=NULL)
	{
		comp_list_t* nova = lista_criaTAC(ptaux->rotulo,ptaux->opCode,ptaux->op1,ptaux->op2,ptaux->alvo1,ptaux->alvo2);
		nova->ant = ant;
		nova->prox = lista_copiaCompleta(nova,ptaux->prox);

		return nova;
	}
	return NULL;
}

/**
 * Imprime na tela (printf) conteudo de um lista
 * name: lista_imprime
 * @param l: lista que ser� impressa
 * @return
 *
 */
void lista_imprime(comp_list_t* l){
    comp_list_t *ptaux = l;
    printf("\n --- LISTA: ---\n");
    while(ptaux != NULL){
		if(strcmp(ptaux->rotulo,"")!=0)
			printf("Rotulo: %s\n",ptaux->rotulo);
        printf("OpCode: %s\n", ptaux->opCode);
		if(strcmp(ptaux->op1,"")!=0)
			printf("Operador 1: %s\n",ptaux->op1);
		if(strcmp(ptaux->op2,"")!=0)
			printf("Operador 2: %s\n",ptaux->op2);

        printf("Alvo1: %s\n", ptaux->alvo1);
        if(strcmp(ptaux->alvo2,"")!=0)
			printf("Alvo2: %s\n", ptaux->alvo2);

        ptaux = ptaux->prox;
    }
    printf(" ---        --- \n\n");
    return;
}
/**
 * Imprime na tela uma lista de TACs
 * name: TAC_imprime
 * @param l: lista de TACs a ser impressa
 *
 */
void TAC_imprime(comp_list_t* l){
    comp_list_t *ptaux = l;
    if(ptaux!=NULL)
    {
		while(ptaux->prox!=NULL)
			ptaux=ptaux->prox;

		while(ptaux != NULL)
		{
			if(strcmp(ptaux->rotulo,"")!=0)
				printf("%s: ",ptaux->rotulo);
			printf("%s ", ptaux->opCode);
			if(strcmp(ptaux->op1,"")!=0)
				printf("%s",ptaux->op1);
			if(strcmp(ptaux->op2,"")!=0)
				printf(",%s",ptaux->op2);
			
			if(strcmp(ptaux->alvo1,"")!=0)
				printf(" => %s", ptaux->alvo1);
			if(strcmp(ptaux->alvo2,"")!=0)
				printf(",%s\n", ptaux->alvo2);
			else
				printf("\n");
			ptaux = ptaux->ant;
		}
    }

    return;
}

//FUNCOES TAC
int registrador = 0;
int label = 0;

/**
 * Cria um tempor�rio (registrador)
 * name: temp
 * @return: nome do registrador
 *
 */
char* temp()
{
	registrador++;
	char* reg = (char*) malloc(sizeof(char)*10);
	strcpy(reg,"r");

	char valor[4] = "";
	sprintf(valor, "%d",registrador);
	strcat(reg,valor);

	//printf("Registrador: %s\n",reg);
	return reg;
}

/**
 * Cria um r�tulo 
 * name: rotulo
 * @return: nome do rotulo
 *
 */
char* rotulo()
{
	label++;
	char* reg = (char*) malloc(sizeof(char)*10);
	strcpy(reg,"L");

	char valor[4] = "";
	sprintf(valor, "%d",label);
	strcat(reg,valor);

	//printf("Registrador: %s\n",reg);
	return reg;
}

/**
 * Cria um rotulo sem alterar a global
 * name: rotulo_verifica
 * @return: nome do rotulo
 *
 */
char* rotulo_verifica()
{
	label++;
	char* reg = (char*) malloc(sizeof(char)*10);
	strcpy(reg,"L");

	char valor[4] = "";
	sprintf(valor, "%d",label);
	strcat(reg,valor);

	//printf("Registrador: %s\n",reg);
	label--;
	return reg;
}

/**
 * Organiza�ao de toda gera�ao de codigo
 * name: TAC_geraCodigo
 * @param arvore: nodo da arvore
 * @return: lista de TACs
 *
 */
comp_list_t* TAC_geraCodigo(comp_tree_t* arvore)
{
	//arvore_imprime(arvore);

	comp_list_t* filhos [MAX_FILHOS] = {NULL,NULL,NULL,NULL};

	if(arvore!=NULL)
	{

		//gera recursivamente � esquerda
		TipoFilhos* auxtf = arvore->filhos;
		int i=0;
		while(auxtf!=NULL)
		{
			filhos[i] = lista_copiaCompleta(NULL,auxtf->arv->TACs);
			auxtf=auxtf->prox;
			i++;
		}

		switch(arvore->tipo_arvore)
		{
			case IKS_AST_LITERAL: { return TAC_geraCodigo_Literal(arvore);};break;
			case IKS_AST_IDENTIFICADOR: { return TAC_geraCodigo_ID(arvore);};break;
			case IKS_AST_ATRIBUICAO: { return TAC_geraCodigo_Atribuicao(arvore,filhos);};break;
			case IKS_AST_ARIM_SOMA: { return TAC_geraCodigo_BinOp(arvore,filhos);};break;
			case IKS_AST_ARIM_SUBTRACAO: { return TAC_geraCodigo_BinOp(arvore,filhos);};break;
			case IKS_AST_ARIM_MULTIPLICACAO: { return TAC_geraCodigo_BinOp(arvore,filhos);};break;
			case IKS_AST_ARIM_DIVISAO: { return TAC_geraCodigo_BinOp(arvore,filhos);};break;
			case IKS_AST_ARIM_INVERSAO: { return TAC_geraCodigo_Inversao(arvore,filhos);};break;
			case IKS_AST_PROGRAMA: { return lista_concatenaFilhos(MAX_FILHOS,filhos); }break;
			case IKS_AST_FUNCAO: { return lista_concatenaFilhos(MAX_FILHOS,filhos); }break;
			case IKS_AST_IF_ELSE: {return TAC_geraCodigo_If(arvore,filhos);}break;
			case IKS_AST_DO_WHILE: {return TAC_geraCodigo_DoWhile(arvore,filhos);}break;
			case IKS_AST_WHILE_DO: {return TAC_geraCodigo_WhileDo(arvore,filhos);}break;
			case IKS_AST_INPUT: {lista_Liberar(filhos[0]);return NULL;}break;
			case IKS_AST_OUTPUT: {lista_Liberar(filhos[0]);return NULL;}break;
			case IKS_AST_RETURN: { lista_Liberar(filhos[0]); return NULL;}break;
			case IKS_AST_BLOCO: {return lista_concatenaFilhos(MAX_FILHOS,filhos);}break;
			case IKS_AST_LOGICO_E: {return TAC_geraCodigo_BoolOp(arvore,filhos);}break;
			case IKS_AST_LOGICO_OU: {return TAC_geraCodigo_BoolOp(arvore,filhos);}break;
			case IKS_AST_LOGICO_COMP_DIF: {return TAC_geraCodigo_BinOp(arvore,filhos);}break;
			case IKS_AST_LOGICO_COMP_IGUAL: {return TAC_geraCodigo_BinOp(arvore,filhos);}break;
			case IKS_AST_LOGICO_COMP_LE: {return TAC_geraCodigo_BinOp(arvore,filhos);}break;
			case IKS_AST_LOGICO_COMP_GE: {return TAC_geraCodigo_BinOp(arvore,filhos);}break;
			case IKS_AST_LOGICO_COMP_L: {return TAC_geraCodigo_BinOp(arvore,filhos);}break;
			case IKS_AST_LOGICO_COMP_G: {return TAC_geraCodigo_BinOp(arvore,filhos);}break;
			case IKS_AST_LOGICO_COMP_NEGACAO: {return TAC_geraCodigo_Negacao(arvore,filhos);}break;
			case IKS_AST_VETOR_INDEXADO: {return TAC_geraCodigo_VetorIndexado(arvore,filhos);}break;
			case IKS_AST_CHAMADA_DE_FUNCAO: {lista_Liberar(filhos[0]);lista_Liberar(filhos[1]);return NULL;}break;
			default: return NULL;
		}
	}
}

/**
 * Gera TAC de um literal
 * name: TAC_geraCodigo_Literal
 * @param arvore: nodo da arvore
 * @return: TAC  do literal
 *
 */
//fazer distin�ao de inteiro, boolean e caractere --> cload load
//carrega constante em registrador
comp_list_t* TAC_geraCodigo_Literal(comp_tree_t* arvore)
{
	
	int tipo = arvore->entrada->tipo;
	
	if(tipo!=IKS_CHAR && tipo!=IKS_BOOL && tipo!=IKS_INT)
		return NULL;
	else
	{
		comp_list_t* operacao =NULL;
		char* rotulo_load = rotulo();
		char* registrador = temp();
		
		//carrega valor direto VERIFICAR BOOLEAN E CARACTERE
		if(tipo==IKS_INT)
			operacao = lista_criaTAC(rotulo_load,loadI, arvore->entrada->chave,SEM_OPERADOR, registrador,SEM_OPERADOR);
		else
			if(tipo==IKS_CHAR)
			{
				int valorASCII = (int) arvore->entrada->chave[0];
				char* asc = (char*) malloc(sizeof(char)*10);
				sprintf(asc, "%d", valorASCII);
		
				//printf("%s -> %s\n",arvore->entrada->chave,asc);
				operacao = lista_criaTAC(rotulo_load,loadI, asc,SEM_OPERADOR, registrador,SEM_OPERADOR);
				free(asc);
			}
			else
				if(strcmp(arvore->entrada->chave,"false")==0)
					operacao = lista_criaTAC(rotulo_load,loadI, "0",SEM_OPERADOR, registrador,SEM_OPERADOR);
				else
					operacao = lista_criaTAC(rotulo_load,loadI,"1",SEM_OPERADOR, registrador,SEM_OPERADOR);
					
			
		free(rotulo_load);
		free(registrador);
		
		return operacao;
	}
	
}

/**
 * Gera TAC de um identificador
 * name: TAC_geraCodigo_ID
 * @param arvore: nodo da arvore
 * @return: TAC do identificador
 *
 */
comp_list_t* TAC_geraCodigo_ID(comp_tree_t* arvore)
{
	//criar UMA TAC
		//loadAI rarp,deslocamento => r1
	//Se nao for variavel retorna NULL
	if(arvore->entrada->tipo_id==TIPO_ID_VARIAVEL){
		//Calcula deslocamento para posicao do id
		char* deslocamento = (char*) malloc(sizeof(char)*10);
		sprintf(deslocamento, "%d", arvore->entrada->deslocamento);
	
		char* registrador = temp(); //novo registrador
		char* rotulo_carrega = rotulo(); //novo rotulo
		
		int qual_escopo = escopo_Local_Global(tabela_simbolos_escopo,arvore->entrada->chave);
		comp_list_t* carrega = NULL;
		if(qual_escopo==1)//local
		//Cria TAC do ID
			carrega = lista_criaTAC(rotulo_carrega,loadAI,rarp,deslocamento, registrador,SEM_OPERADOR);
		else
			carrega = lista_criaTAC(rotulo_carrega,loadAI,bss,deslocamento, registrador,SEM_OPERADOR);
		
		//liberacao das strings j� copiadas
		free(deslocamento);
		free(registrador);
		free(rotulo_carrega);
		
		return carrega;
	}
	else 
		return NULL;
}

/**
 * Gera TAC de uma opera��o artimetica
 * name: TAC_geraCodigo_BinOp
 * @param arvore: nodo da arvore
 * @param filhos: lista de filhos do nodo
 * @return: TAC da opera��o bin�ria
 *
 */
comp_list_t* TAC_geraCodigo_BinOp(comp_tree_t* arvore, comp_list_t** filhos)
{
	//opCode reg1 reg2 => regnovo
	
	char* op2 = filhos[1]->alvo1;
	char* op1 = filhos[0]->alvo1;

	char* rotulo_operacao = rotulo();
	
	char* registrador = temp();
	
	comp_list_t* operacao = lista_criaTAC(rotulo_operacao,IKS_AST_para_opCode(arvore->tipo_arvore), op1,op2, registrador,SEM_OPERADOR);

	comp_list_t* join = lista_concatenaFilhos(MAX_FILHOS,filhos);
	operacao = lista_concatena(operacao,join);

	
	free(rotulo_operacao);
	free(registrador);

	return operacao;
}

/**
 * Gera TAC de uma opera��o booleana
 * name: TAC_geraCodigo_BinOp
 * @param arvore: nodo da arvore
 * @param filhos: lista de filhos do nodo
 * @return: TAC da opera��o bin�ria
 *
 */
comp_list_t* TAC_geraCodigo_BoolOp(comp_tree_t* arvore, comp_list_t** filhos)
{
	//char* op2 = filhos[0]->alvo1;
	char* op1 = filhos[0]->alvo1;
	//OU
	if(arvore->tipo_arvore==IKS_AST_LOGICO_OU)
	{
		//cbr op1 => nop_saida,op2_label
		char* rotulo_operacao = rotulo();
		char* rotulo_saida = rotulo_verifica();
		char* registrador = temp();
		
		
		comp_list_t* primeiro_com = filhos[1];
		while(primeiro_com->prox!=NULL)
			primeiro_com=primeiro_com->prox;
			
		
		
		char* rotulo_prox = primeiro_com->rotulo;
		
		comp_list_t* operacao = lista_criaTAC(rotulo_operacao,"cbr", op1,SEM_OPERADOR,"Lt",rotulo_prox);
		
		comp_list_t* join = lista_concatena(filhos[1],operacao);
		join = lista_concatena(join,filhos[0]);
		
		free(rotulo_operacao);
		free(rotulo_saida);
		free(registrador);
		
		return join;
	}
	else
	{
		//cbr op1 => op2_label,nop_saida
		char* rotulo_operacao = rotulo();
		char* rotulo_saida = rotulo_verifica();
		char* registrador = temp();
		
		
		comp_list_t* primeiro_com = filhos[1];
		while(primeiro_com->prox!=NULL)
			primeiro_com=primeiro_com->prox;
			
		
		
		char* rotulo_prox = primeiro_com->rotulo;
		
		comp_list_t* operacao = lista_criaTAC(rotulo_operacao,"cbr", op1,SEM_OPERADOR,rotulo_prox,"Lf");
		
		comp_list_t* join = lista_concatena(filhos[1],operacao);
		join = lista_concatena(join,filhos[0]);
		
		free(rotulo_operacao);
		free(rotulo_saida);
		free(registrador);
		
		return join;
		
	}
	
	


}


/**
 * Gera TAC de uma atribui��o
 * name: TAC_geraCodigo_Atribuicao
 * @param arvore: nodo da arvore
 * @param filhos: lista de filhos do nodo
 * @return: Lista com TAC da atribui��o adicionado
 *
 */
comp_list_t* TAC_geraCodigo_Atribuicao(comp_tree_t* arvore, comp_list_t** filhos)
{
	if(filhos[1]==NULL) //chamada de funcao
	{
		lista_Liberar(filhos[0]);
		return NULL;
	}
	//STORE
		//(c)storeAI r1 => rarp,deslocamento
	
	//procura primeiro filho q tem o deslocamento
	comp_list_t* aux = filhos[0];
	while(aux->prox!=NULL)
		aux = aux->prox;
	
	//verifica o deslocamento passado para o load
	int desloca = atoi(aux->op2);
	char* deslocamento = (char*) malloc(sizeof(char)*6);
	sprintf(deslocamento, "%d", desloca);
	
	char* resultado = filhos[1]->alvo1;

	comp_list_t* operacao = NULL;
	//storeAI resultado => deslocamento,base
	
	
	char* rotulo_store = rotulo();
	//verifica se � variavel
	if(arvore->filhos->arv->tipo_arvore == IKS_AST_IDENTIFICADOR)
		if(arvore->filhos->arv->tipo==IKS_CHAR) //primeiro filho j� contem identificador
		{
			char* rotulo_ic = rotulo();
			char* reg_ic = temp();
			
			comp_list_t* inteiro_char = lista_criaTAC(rotulo_ic,"i2c",resultado,SEM_OPERADOR,reg_ic,SEM_OPERADOR);
			
			int qual_escopo = escopo_Local_Global(tabela_simbolos_escopo,arvore->filhos->arv->entrada->chave);
			if(qual_escopo==1)
				operacao = lista_criaTAC(rotulo_store,cstoreAI,inteiro_char->alvo1,SEM_OPERADOR,rarp,deslocamento); //se tipo char, store especial
			else
				operacao = lista_criaTAC(rotulo_store,cstoreAI,inteiro_char->alvo1,SEM_OPERADOR,bss,deslocamento);
			
			operacao = lista_concatena(operacao,inteiro_char);
			
			free(rotulo_ic);
			free(reg_ic);
		}
		else
		{
			int qual_escopo = escopo_Local_Global(tabela_simbolos_escopo,arvore->filhos->arv->entrada->chave);
			if(qual_escopo==1)
				operacao = lista_criaTAC(rotulo_store,storeAI,resultado,SEM_OPERADOR,rarp,deslocamento);
			else
				operacao = lista_criaTAC(rotulo_store,storeAI,resultado,SEM_OPERADOR,bss,deslocamento);
		}
	//ou acesso a vetor indexado
	else
		if(arvore->filhos->arv->filhos->arv->tipo==IKS_CHAR) //primeiro filho do primeiro filho contem identificador
		{
			char* rotulo_ic = rotulo();
			char* reg_ic = temp();
			comp_list_t* inteiro_char = lista_criaTAC(rotulo_ic,"i2c",resultado,SEM_OPERADOR,reg_ic,SEM_OPERADOR);
			
			int qual_escopo = escopo_Local_Global(tabela_simbolos_escopo,arvore->filhos->arv->filhos->arv->entrada->chave);
			if(qual_escopo==1)
				operacao = lista_criaTAC(rotulo_store,cstoreAI,inteiro_char->alvo1,SEM_OPERADOR,rarp,deslocamento); //se tipo char, store especial
			else
				operacao = lista_criaTAC(rotulo_store,cstoreAI,inteiro_char->alvo1,SEM_OPERADOR,bss,deslocamento);
				
			operacao = lista_concatena(operacao,inteiro_char);
			
			free(rotulo_ic);
			free(reg_ic);
		}
		else
		{
			int qual_escopo = escopo_Local_Global(tabela_simbolos_escopo,arvore->filhos->arv->filhos->arv->entrada->chave);
			if(qual_escopo==1)
				operacao = lista_criaTAC(rotulo_store,storeAI,resultado,SEM_OPERADOR,rarp,deslocamento);
			else
				operacao = lista_criaTAC(rotulo_store,storeAI,resultado,SEM_OPERADOR,bss,deslocamento);
		}
	

	comp_list_t* join = lista_concatenaFilhosSemPrimeiro(MAX_FILHOS,filhos);
	
		
	operacao = lista_concatena(operacao,join);
	
	comp_list_t* ptaux = operacao;
	int store1=0,store0=0;
	char* rotulo_carrf = NULL;
	char* rotulo_carrt = NULL;
	
	while(ptaux!=NULL)
	{
		if(strcmp(ptaux->alvo1,"Lt")==0)
		{
			if(store1==0)
			{
				store1=1;
				rotulo_carrt = rotulo();
				char* reg_carr = temp();
				char* rotulo_nop = rotulo();
				char* rotulo_jump = rotulo();
				
				comp_list_t* carr = lista_criaTAC(rotulo_carrt,loadI,"1",SEM_OPERADOR,reg_carr,SEM_OPERADOR);
				comp_list_t* storeTrue = lista_criaTAC(rotulo_store,storeAI,reg_carr,SEM_OPERADOR,rarp,deslocamento);
				
				comp_list_t* nop_saida = lista_criaTAC(rotulo_nop,"nop",SEM_OPERADOR,SEM_OPERADOR,SEM_OPERADOR,SEM_OPERADOR);
				comp_list_t* jump_saida = lista_criaTAC(rotulo_jump,"jumpI",SEM_OPERADOR,SEM_OPERADOR,rotulo_nop,SEM_OPERADOR);
				
				
				operacao = lista_concatena(jump_saida,operacao);
				
				storeTrue = lista_concatena(storeTrue,carr);		
				
				operacao = lista_concatena(storeTrue,operacao);
				operacao = lista_concatena(nop_saida,operacao);
				
			
				free(rotulo_nop);
				free(rotulo_jump);
				free(reg_carr);
				
			}
			free(ptaux->alvo1);
			ptaux->alvo1=string_copia(rotulo_carrt);
			
		}		
		else if(strcmp(ptaux->alvo2,"Lf")==0)
		{
			if(store0==0)
			{
				store0=1;
				rotulo_carrf = rotulo();
				char* reg_carr = temp();
				char* rotulo_nop = rotulo();
				char* rotulo_jump = rotulo();
				
				comp_list_t* carr = lista_criaTAC(rotulo_carrf,loadI,"0",SEM_OPERADOR,reg_carr,SEM_OPERADOR);
				comp_list_t* storeTrue = lista_criaTAC(rotulo_store,storeAI,reg_carr,SEM_OPERADOR,rarp,deslocamento);
				
				comp_list_t* nop_saida = lista_criaTAC(rotulo_nop,"nop",SEM_OPERADOR,SEM_OPERADOR,SEM_OPERADOR,SEM_OPERADOR);
				comp_list_t* jump_saida = lista_criaTAC(rotulo_jump,"jumpI",SEM_OPERADOR,SEM_OPERADOR,rotulo_nop,SEM_OPERADOR);
				
				
				operacao = lista_concatena(jump_saida,operacao);
				
				storeTrue = lista_concatena(storeTrue,carr);		
				
				operacao = lista_concatena(storeTrue,operacao);
				operacao = lista_concatena(nop_saida,operacao);

				free(rotulo_nop);
				free(rotulo_jump);
				free(reg_carr);
				
			}
				free(ptaux->alvo2);
				ptaux->alvo2=string_copia(rotulo_carrf);
				
				
				
			
		}
		
		ptaux = ptaux->prox;
	}
	free(rotulo_carrt);
	free(rotulo_carrf);
		

	
	free(rotulo_store);
	
	
	//lista_Liberar(filhos[0]);
	if(arvore->filhos->arv->tipo_arvore==IKS_AST_VETOR_INDEXADO && arvore->filhos->arv->filhos->prox->arv->tipo_arvore!=IKS_AST_LITERAL)
	{
		free(operacao->opCode);
		operacao->opCode = "storeAO";
		
		free(operacao->alvo2);
		operacao->alvo2 = (char*) malloc(strlen(filhos[0]->alvo1)+1);
		
		
		char* rot_soma = rotulo();
		char* reg_soma = temp();
		char* desloc_soma = (char*) malloc(sizeof(char)*10);
		sprintf(desloc_soma, "%d", arvore->filhos->arv->filhos->arv->entrada->deslocamento);
		
		comp_list_t* soma_desloc = lista_criaTAC(rot_soma,"add",filhos[0]->alvo1,desloc_soma,reg_soma,SEM_OPERADOR);
		
		strcpy(operacao->alvo2,reg_soma);
		operacao = lista_concatena(operacao,soma_desloc);
		operacao = lista_concatena(operacao,filhos[0]);
		
		free(rot_soma);
		free(reg_soma);
		free(desloc_soma);
	}
	else
		lista_Liberar(filhos[0]);
		
	free(deslocamento);
	return operacao;
	

}

/**
 * Gera TAC de uma opera��o de invers�o
 * name: TAC_geraCodigo_Inversao
 * @param arvore: nodo da arvore
 * @param filhos: lista de filhos do nodo
 * @return: Lista com TAC da invers�o adicionada
 *
 */
comp_list_t* TAC_geraCodigo_Inversao(comp_tree_t* arvore, comp_list_t** filhos)
{

	//fazer inversao, realiza subtracao de zero
	char* rotulo_zero = rotulo();
	char* reg_zero = temp();
	
	comp_list_t* zero = lista_criaTAC(rotulo_zero,loadI,"0",SEM_OPERADOR,reg_zero,SEM_OPERADOR);


	char* op1 = zero->alvo1;
	char* op2 = filhos[0]->alvo1;

	char* rotulo_op = rotulo();
	char* reg_op = temp();
	
	comp_list_t* operacao = lista_criaTAC(rotulo_op,IKS_AST_para_opCode(arvore->tipo_arvore), op1,op2, reg_op,SEM_OPERADOR);

	comp_list_t* join = lista_concatenaFilhos(MAX_FILHOS,filhos);


	//insere zero na lista de filhos
	comp_list_t* ptaux = join;
	while(ptaux->prox!=NULL)
		ptaux=ptaux->prox;
	ptaux->prox = zero;
	zero->ant = ptaux;

	operacao = lista_concatena(operacao,join);

	free(rotulo_zero);
	free(rotulo_op);
	free(reg_zero);
	free(reg_op);
	
	return operacao;
}

/**
 * Gera TAC de uma opera��o de nega��o
 * name: TAC_geraCodigo_Negacao
 * @param arvore: nodo da arvore
 * @param filhos: lista de filhos do nodo
 * @return: Lista com TAC da negacao adicionada
 *
 */
comp_list_t* TAC_geraCodigo_Negacao(comp_tree_t* arvore, comp_list_t** filhos){

    char* rotulo_true = rotulo();
    char* reg_true = temp();
    
    comp_list_t* true = lista_criaTAC(rotulo_true,loadI,"1",SEM_OPERADOR,reg_true,SEM_OPERADOR);

    char* op1 = true->alvo1;
    char* op2 = filhos[0]->alvo1;

	char* rotulo_op = rotulo();
	char* reg_op = temp();
	
    comp_list_t* operacao = lista_criaTAC(rotulo_op,IKS_AST_para_opCode(arvore->tipo_arvore), op1,op2, reg_op,SEM_OPERADOR);
    comp_list_t* join = lista_concatenaFilhos(MAX_FILHOS,filhos);


    //insere zero na lista de filhos
    comp_list_t* ptaux = join;
    while(ptaux->prox!=NULL)
        ptaux = ptaux->prox;

    ptaux->prox = true;
    true->ant = ptaux;


    operacao = lista_concatena(operacao,join);
    
    free(rotulo_true);
    free(rotulo_op);
    free(reg_true);
    free(reg_op);

    return operacao;
}

/**
 * Gera TAC de um if
 * name: TAC_geraCodigo_If
 * @param arvore: nodo da arvore
 * @param filhos: lista de filhos do nodo
 * @return: Lista com TAC do if adicionado
 *
 */
comp_list_t* TAC_geraCodigo_If(comp_tree_t* arvore, comp_list_t** filhos)
{
	/*if (expr) then com1 else com2
	* crb expr->alvo1 => com1->rotulo, com2->rotulo (adicionar jump no cm2)
	* sem else:
	* crb expr->alvo1 => com1->rotulo, nop->rotulo (adicionario jump)
	* --> operacao nop no final para pulo do else 
	*/
	
	char* expr = filhos[0]->alvo1; //pega resultado da expressao condicional
	//atualiza rotulos de jumps de possivel expressao booleana curto circuito
	char* rotulo_nop_saida_if = rotulo();
	comp_list_t* nop_saida_if = lista_criaTAC(rotulo_nop_saida_if,"nop",SEM_OPERADOR,SEM_OPERADOR,SEM_OPERADOR,SEM_OPERADOR);
		
	
	//busca rotulo do primeiro comando para then
	comp_list_t* aux = filhos[1];
	while(aux->prox!=NULL)
		aux=aux->prox;
	char* thenf = aux->rotulo; //pega rotulo do then (para onde crb deve ir se expr for true
	
	//if SEM else
	if(arvore->filhos_proprios == 2)
	{
		char* rotulo_op = rotulo();
		comp_list_t* operacao = lista_criaTAC(rotulo_op,"crb",expr,SEM_OPERADOR,thenf,rotulo_nop_saida_if);
		
		comp_list_t* join = lista_concatena(nop_saida_if,filhos[1]); //coloca operacao nop apos todas do if, para salto do else
		
		operacao = lista_concatena(join,operacao);
		operacao = lista_concatena(operacao,filhos[0]); //expressao tem q vir antes do if
		
		
		operacao = TAC_curtoCircuito_Rotulo(operacao,thenf,rotulo_nop_saida_if);
		
		free(rotulo_op);
		free(rotulo_nop_saida_if);
		
		return operacao;
		
	}
	//if COM else
	else
	{
		comp_list_t* aux2 = filhos[2];
		while(aux2->prox!=NULL)
			aux2=aux2->prox;
		char* elsef = aux2->rotulo; //pega rotulo do then (para onde crb deve ir se expr for true
		
		char* rotulo_op = rotulo();
		comp_list_t* operacao = lista_criaTAC(rotulo_op,"crb",expr,SEM_OPERADOR,thenf,elsef);
		
		char* rotulo_jump = rotulo();
		comp_list_t* jump_else = lista_criaTAC(rotulo_jump,"jumpI",SEM_OPERADOR,SEM_OPERADOR,rotulo_nop_saida_if,SEM_OPERADOR);
		
		
		jump_else = lista_concatena(jump_else,filhos[2]);
		jump_else = lista_concatena(filhos[1],jump_else);
		jump_else = lista_concatena(nop_saida_if,jump_else); //coloca operacao nop apos todas do if, para salto do else
		
		operacao = lista_concatena(jump_else,operacao);
		operacao = lista_concatena(operacao,filhos[0]); //expressao tem q vir antes do if
		
		operacao = TAC_curtoCircuito_Rotulo(operacao,thenf,rotulo_nop_saida_if);
		
		free(rotulo_op);
		free(rotulo_jump);
		free(rotulo_nop_saida_if);
		
		return operacao;
	}	
	
}

/**
 * Atualiza r�tulo do curto circuito para label do comando apropriado
 * name: TAC_curtoCircuito_Rotulo
 * @param lista: lista anterior
 * @param novot: label para store de true
 * @param novof: label para store de false
 * @return: Lista com TAC com r�tulo atualizado
 *
 */
comp_list_t* TAC_curtoCircuito_Rotulo(comp_list_t* saida, char* novot, char* novof)
{
	comp_list_t* ptaux = saida;
	while(ptaux!=NULL)
	{
		if(strcmp(ptaux->alvo1,"Lt")==0)
		{
			free(ptaux->alvo1);
			ptaux->alvo1 = string_copia(novot);
		}		
		if(strcmp(ptaux->alvo2,"Lf")==0)
		{
			free(ptaux->alvo2);
			ptaux->alvo2 = string_copia(novof);
		}
		ptaux=ptaux->prox;
	}
	
	return saida;
}

/**
 * Gera TAC de um while do
 * name: TAC_geraCodigo_WhileDo
 * @param arvore: nodo da arvore
 * @param filhos: lista de filhos do nodo
 * @return: Lista com TAC do while do adicionado
 *
 */
comp_list_t* TAC_geraCodigo_WhileDo(comp_tree_t* arvore, comp_list_t** filhos)
{
	
	/* while(cond) do comando
	 * cbr cond->alvo1 => comando->rotulo,nop_saida->rotulo
	 * comando incluir jump para cbr anterior
	 */
	 
	comp_list_t* ptaux = filhos[0];
	while(ptaux->prox!=NULL)
		ptaux=ptaux->prox;
	char* expr = filhos[0]->alvo1; //pega resultado da expressao condicional
	
	
	//busca rotulo do primeiro comando do comando while
	comp_list_t* aux = filhos[1];
	while(aux->prox!=NULL)
		aux=aux->prox;
	char* com = aux->rotulo; //pega rotulo do comando 
	
	char* rotulo_jump = rotulo();
	char* rotulo_cbr = rotulo();
	
	//jump para while
	comp_list_t* jump_while = lista_criaTAC(rotulo_jump,"jumpI",SEM_OPERADOR,SEM_OPERADOR,ptaux->rotulo,SEM_OPERADOR);
	
	//nop de saida do while
	char* rotulo_nop_saida_while = rotulo();
	comp_list_t* nop_saida_while = lista_criaTAC(rotulo_nop_saida_while,"nop",SEM_OPERADOR,SEM_OPERADOR,SEM_OPERADOR,SEM_OPERADOR);
	
	char* saida_while = nop_saida_while->rotulo;
	
	//operacao do while, verdadeiro -> rotulo comando, false -> rotulo nop de saida do while
	comp_list_t* operacao = lista_criaTAC(rotulo_cbr,"crb",expr,SEM_OPERADOR,com,saida_while);
	
	//coloca jump logo ap�s o comando
	comp_list_t* join = lista_concatena(jump_while,filhos[1]);
	
	//coloca nop_saida_while ap�s o jump
	join = lista_concatena(nop_saida_while,join);
	
	join = lista_concatena(join,operacao);
	join = lista_concatena(join,filhos[0]);
	
	join = TAC_curtoCircuito_Rotulo(join,com,saida_while);
	
	free(rotulo_jump);
	free(rotulo_cbr);
	free(rotulo_nop_saida_while);
			
	
	return join;
		
	
}

/**
 * Gera TAC de um do while 
 * name: TAC_geraCodigo_DoWhile
 * @param arvore: nodo da arvore
 * @param filhos: lista de filhos do nodo
 * @return: Lista com TAC do do while  adicionado
 *
 */
comp_list_t* TAC_geraCodigo_DoWhile(comp_tree_t* arvore, comp_list_t** filhos)
{
	
	/* do comando while(cond)
	 * cbr cond->alvo => comando->rotulo, nop_saida
	 */
	 
	 comp_list_t* ptaux = filhos[1];
	while(ptaux->prox!=NULL)
		ptaux=ptaux->prox;
	char* expr = filhos[1]->alvo1; //pega resultado da expressao condicional
	
	
	//busca rotulo do primeiro comando do comando while
	comp_list_t* aux = filhos[0];
	while(aux->prox!=NULL)
		aux=aux->prox;
	char* com = aux->rotulo; //pega rotulo do comando 
	
	
	
	char* rotulo_saida_while = rotulo();
	//nop de saida do while
	comp_list_t* nop_saida_while = lista_criaTAC(rotulo_saida_while,"nop",SEM_OPERADOR,SEM_OPERADOR,SEM_OPERADOR,SEM_OPERADOR);
	char* saida_while = nop_saida_while->rotulo;
	
	char* rotulo_op = rotulo();
	//operacao do while, verdadeiro -> rotulo comando, false -> rotulo nop de saida do while
	comp_list_t* operacao = lista_criaTAC(rotulo_op,"crb",expr,SEM_OPERADOR,com,saida_while);
	
	
	nop_saida_while = lista_concatena(nop_saida_while,operacao);
	operacao = lista_concatena(nop_saida_while,filhos[1]);
	comp_list_t* join = lista_concatena(operacao,filhos[0]);

	join = TAC_curtoCircuito_Rotulo(join,com,saida_while);
	
	free(rotulo_saida_while);
	free(rotulo_op);
	
	
	return join;
		
	
}

/**
 * Gera TAC de um vetor indexado
 * name: TAC_geraCodigo_VetorIndexado
 * @param arvore: nodo da arvore
 * @param filhos: lista de filhos do nodo
 * @return: Lista com TAC vetor indexado adicionado
 *
 */
comp_list_t* TAC_geraCodigo_VetorIndexado(comp_tree_t* arvore, comp_list_t** filhos)
{

	//verifica tamanho do tipo do dado (primeiro filho � o identificador)
	int tamanho = TAMANHO_POR_TIPO(arvore->filhos->arv->entrada->tipo);
	int desloc_ID = arvore->filhos->arv->entrada->deslocamento;
	
	//verifica indice do array (segundo filho)
	comp_tree_t* arvore_indices = arvore->filhos->prox->arv;
	comp_dict_t* dicio_id = arvore->filhos->arv->entrada;
	int dimensao = arvore->filhos->arv->entrada->vetor->dimensao;
	
	if(arvore_indices->tipo_arvore==IKS_AST_LITERAL)
	{
		int posicao = deslocamento_arranjos(arvore_indices,dimensao,dicio_id);
		char* deslocamento = (char*) malloc(sizeof(char)*6);
		sprintf(deslocamento, "%d", desloc_ID + tamanho*posicao);
		//printf("deslocamento %d %d %d\n",arvore->filhos->arv->entrada->deslocamento,tamanho,posicao);
		
		char* rotulo_carrega = rotulo();
		char* reg_carrega = temp();
		//Cria TAC do ID
		comp_list_t* carrega = lista_criaTAC(rotulo_carrega,loadAI,rarp,deslocamento, reg_carrega,SEM_OPERADOR);
		
		free(rotulo_carrega);
		free(reg_carrega);
		free(deslocamento);
		
		int i;
		for(i=0;i<MAX_FILHOS;i++)
			lista_Liberar(filhos[i]);
		
		return carrega;
	}
	else
	{
		char* rot_deslocreg = rotulo();
		char* reg_deslocreg = temp();
		
		char* tamanho_string[10];
		sprintf(tamanho_string, "%d",tamanho);
		comp_list_t* desloc_reg = lista_criaTAC(rot_deslocreg,"mult",arvore_indices->TACs->alvo1,tamanho_string,reg_deslocreg,SEM_OPERADOR);
		
		char* rotulo_carrega = rotulo();
		char* reg_carrega = temp();
		//Cria TAC do ID
		comp_list_t* carrega = lista_criaTAC(rotulo_carrega,loadAI,rarp,reg_deslocreg, reg_carrega,SEM_OPERADOR);
		
		free(rotulo_carrega);
		free(reg_carrega);
		free(rot_deslocreg);
		free(reg_deslocreg);
		
		int i;
		//for(i=0;i<MAX_FILHOS;i++)
			//lista_Liberar(filhos[i]);
			
		carrega = lista_concatenaFilhos(MAX_FILHOS,filhos);
		
		return carrega;
	}
		
	
	
	
}

/**
 * Calcula o deslocamento de uma posicao de um vetor
 * name: deslocamento_arranjos
 * @param arvore_indices: nodo da arvore
 * @param id_vetor: vetor para o calculo
 * @return: Deslocamento calculado
 *
 */
int deslocamento_arranjos(comp_tree_t* arvore_indices, int k, comp_dict_t* id_vetor)
{
	//dicionario_imprime(id_vetor);
	
	int indice_atual =0;
	printf("tip: %d\n",arvore_indices->tipo_arvore);
	if(arvore_indices->tipo_arvore == IKS_AST_IDENTIFICADOR)
		indice_atual = atoi(arvore_indices->filhos->arv->entrada->chave);
	else
		indice_atual = atoi(arvore_indices->entrada->chave);
	
	if(k==1)
		return indice_atual;
	else
	{
		int dk_ant = deslocamento_arranjos(arvore_indices->filhos->arv,k-1,id_vetor);
		int n_elementosK = dicionario_elementosDimensaoK(k-1,id_vetor);
		
		//printf("K = %d -- dk_ant = %d -- nK = %d -- ik = %d retornando: %d\n",k,dk_ant,n_elementosK,indice_atual,dk_ant*n_elementosK+indice_atual);
		return dk_ant*n_elementosK + indice_atual;
	}	
}
