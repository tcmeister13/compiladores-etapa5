/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "comp_dict.h"
#include "stringf.h"
#include "define.h"


/**
 * Aloca um dicionário
 * name: dicionario_Alocar
 * @param
 * @return NULL
 *
 */
comp_dict_t* dicionario_Alocar (){
	comp_dict_t* novo = (comp_dict_t*) malloc(sizeof(comp_dict_t));
	return novo;
}

/**
 * Desaloca um dicionário
 * name: dicionario_Liberar
 * @param dicio: dicionario a ser desalocado
 * @return
 *
 */
void dicionario_Liberar(comp_dict_t* dicio)
{
	comp_dict_t* aux = dicio;
	comp_dict_t* liberar = dicio;
	while(aux!=NULL)
	{
		aux = aux->prox;
		free(liberar->chave);
		if(liberar->funcao!=NULL)
		{
			tipo_param* auxp = liberar->funcao->param;
			tipo_param* liberarp = liberar->funcao->param;
			while(auxp!=NULL)
			{
				auxp = auxp->prox;
				free(liberarp);
				liberarp=auxp;
			}
			free(liberar->funcao);
		}
		if(liberar->vetor!=NULL)
		{
			tam_dimensao* auxd = liberar->vetor->dimensoes;
			tam_dimensao* liberard = liberar->vetor->dimensoes;
			while(auxd!=NULL)
			{
				auxd=auxd->prox;
				free(liberard);
				liberard=auxd;
			}
			free(liberar->vetor);
		}
		free(liberar);
		liberar=aux;
	}
}

/**
 * Cria um dicionário de uma entrada
 * name: dicionario_CriarCopiaUnica
 * @param dicionario: entrada a ser copiada
 * @return entrada de um dicionario
 *
 */
comp_dict_t* dicionario_CriaCopiaUnica (comp_dict_t* dicionario)
{
	comp_dict_t* novo ;

	if(dicionario==NULL)
		novo = NULL;
	else
	{
		novo = dicionario_Alocar();
		novo->linha = dicionario->linha;
		novo->tamanho = dicionario->tamanho;
		novo->deslocamento = dicionario->deslocamento;
		novo->tipo = dicionario->tipo;
		novo->tipo_id = dicionario->tipo_id;

		if(dicionario->funcao!=NULL)
		{
			novo->funcao=(tipo_funcao*) malloc(sizeof(tipo_funcao));
				novo->funcao->tipo_retorno = dicionario->funcao->tipo_retorno;
				novo->funcao->n_param = dicionario->funcao->n_param;

				if(dicionario->funcao->param!=NULL)
				{
					tipo_param* aux = dicionario->funcao->param;
					tipo_param* novo_param = NULL;
					while(aux!=NULL)
					{
						novo_param = dicionario_incluiParametroFinal(aux->tipo,novo_param);
						aux=aux->prox;
					}
					novo->funcao->param=novo_param;
				}
				else
					novo->funcao->param = NULL;

		}
		else
			novo->funcao = NULL;

		if(dicionario->vetor!=NULL)
		{
			novo->vetor = (tipo_vetor*)malloc(sizeof(tipo_vetor));
			novo->vetor->dimensao = dicionario->vetor->dimensao;
			novo->vetor->tamanho = dicionario->vetor->tamanho;
			
			if(dicionario->vetor->dimensoes!=NULL)
			{
				tam_dimensao* aux = dicionario->vetor->dimensoes;
				tam_dimensao* nova_dim = NULL;
				while(aux!=NULL)
				{
					nova_dim = dicionario_incluiDimensaoFinal(aux->tamanho,nova_dim);
					aux=aux->prox;
				}
				novo->vetor->dimensoes=nova_dim;				
			}
			else
				novo->vetor->dimensoes = NULL;
		}
		else
			novo->vetor = NULL;

		novo->prox = NULL;
		novo->chave = string_copia(dicionario->chave);
	}

	return novo;


}

/**
 * Inclui um parametro na lista de parametros de uma função, auxiliar a copia
 * name: dicionario_incluiParametroFinal
 * @param tipo: tipo a ser adicionado
 * @param add: lista de parametros a ter novo tipo
 * @return lista de parametros atualizada
 *
 */
tipo_param* dicionario_incluiParametroFinal(int tipo, tipo_param* add)
{
	tipo_param* novo = (tipo_param*) malloc(sizeof(tipo_param));
	novo->tipo = tipo;
	novo->prox = NULL;

	tipo_param* aux = add;

	if(aux==NULL)
		return novo;

	while(aux->prox!=NULL)
		aux=aux->prox;

	aux->prox=novo;

	return add;
}

/**
 * Inclui uma dimensao no final de um tipo vetor
 * name: dicionario_incluiDimensaoFinal
 * @param tam: tamanho a ser adicionado
 * @param add: lista de dimensoes
 * @return lista de dimensoes atualizada
 *
 */
tam_dimensao* dicionario_incluiDimensaoFinal(int tam, tam_dimensao* add)
{
	tam_dimensao* novo = (tam_dimensao*) malloc(sizeof(tam_dimensao));
	novo->tamanho = tam;
	novo->prox = NULL;

	tam_dimensao* aux = add;

	if(aux==NULL)
		return novo;

	while(aux->prox!=NULL)
		aux=aux->prox;

	aux->prox=novo;

	return add;
}


/**
 * Cria um dicionário a partir do dado
 * name: dicionario_CriarCopiaCompleta
 * @param dicionario: entrada a ser copiada
 * @return entrada de um dicionario
 *
 */
comp_dict_t* dicionario_CriaCopiaCompleta (comp_dict_t* dicionario)
{
	comp_dict_t* novo;

	if(dicionario==NULL)
		novo = NULL;
	else
	{
		novo = dicionario_Alocar();
		novo->linha = dicionario->linha;
		novo->tamanho = dicionario->tamanho;
		novo->deslocamento = dicionario->deslocamento;
		novo->tipo = dicionario->tipo;
		novo->tipo_id = dicionario->tipo_id;
		if(dicionario->funcao!=NULL)
		{
			novo->funcao=(tipo_funcao*) malloc(sizeof(tipo_funcao));
				novo->funcao->tipo_retorno = dicionario->funcao->tipo_retorno;
				novo->funcao->n_param = dicionario->funcao->n_param;

				if(dicionario->funcao->param!=NULL)
				{
					tipo_param* aux = dicionario->funcao->param;
					tipo_param* novo_param = NULL;
					while(aux!=NULL)
					{
						novo_param = dicionario_incluiParametroFinal(aux->tipo,novo_param);
						aux=aux->prox;
					}
					novo->funcao->param=novo_param;
				}
				else
					novo->funcao->param = NULL;

		}
		else
			novo->funcao = NULL;

		if(dicionario->vetor!=NULL)
		{
			novo->vetor = (tipo_vetor*)malloc(sizeof(tipo_vetor));
			novo->vetor->dimensao = dicionario->vetor->dimensao;
			novo->vetor->tamanho = dicionario->vetor->tamanho;
			
			if(dicionario->vetor->dimensoes!=NULL)
			{
				tam_dimensao* aux = dicionario->vetor->dimensoes;
				tam_dimensao* nova_dim = NULL;
				while(aux!=NULL)
				{
					nova_dim = dicionario_incluiDimensaoFinal(aux->tamanho,nova_dim);
					aux=aux->prox;
				}
				novo->vetor->dimensoes=nova_dim;				
			}
			else
				novo->vetor->dimensoes = NULL;
		}
		else
			novo->vetor = NULL;

		novo->prox = NULL;
		novo->chave = string_copia(dicionario->chave);

		novo->chave = string_copia(dicionario->chave);
		novo->prox = dicionario_CriaCopiaCompleta(dicionario->prox);

	}

	return novo;


}

/**
 * Adiciona nova entrada no dicionário
 * name: dicionario_AdicionarEntrada
 * @param Chave: chave da nova entrada
 * @param dado: informação da entrada
 * @param Dicionario: diconário no qual deve ser adicionado a entrada
 * @return Dicionario atualizado
 *
 */
comp_dict_t* dicionario_AdicionaEntrada (char* Chave, int linha, int tipo, comp_dict_t* Dicionario)
{
	comp_dict_t* novo;

	novo = dicionario_Alocar();

	novo->linha = linha;
	novo->tipo = tipo;
	novo->tipo_id = -1;
	novo->funcao = NULL;
	novo->vetor = NULL;
	novo->prox = NULL;


	novo->chave = string_copia(Chave);

	//tamanho
	novo->tamanho = TAMANHO_POR_TIPO(tipo);
	if(tipo==IKS_STRING)
		novo->tamanho = strlen(novo->chave)*novo->tamanho;

	comp_dict_t* ant = NULL;
	comp_dict_t* ptaux = Dicionario;
	int menor = 1;
	int igual = 0;
	int maior = 0;


	//procura posição de inserção (em ordem alfabetica)
	while(ptaux!=NULL && menor==1)
	{
		if(strcmp(ptaux->chave, Chave)<0) //se menor, continua a busca
		{ menor = 1; igual = 0; maior = 0; ant = ptaux; ptaux = ptaux->prox; }
		else
			if(strcmp(ptaux->chave, Chave)==0)
			{ menor = 0; igual = 1; maior = 0; }
			else { menor = 0; igual = 0; maior = 1; }
	}

	if(igual==1)
	{ ptaux->linha = linha; dicionario_Liberar(novo); return Dicionario; }

	if(ant==NULL)
	{
		novo->prox = Dicionario;
		return novo;
	}
	else
	{
		novo->prox = ant->prox;
		ant->prox = novo;

		return Dicionario;
	}

}


/**
 * Retorna a entrada do dicionário
 * name: dicionario_AlterarEntrada
 * @param Chave: chave da entrada solicitada
 * @param Dicionario: dicionário na qual está a entrada
 * @return Entrada da chave
 *
 */
comp_dict_t* dicionario_LocalizarEntrada (char* Chave, comp_dict_t* Dicionario){

	comp_dict_t *ptaux = Dicionario; //ponteiro auxiliar para percorrer a lista

	 /*procura o elemento na lista*/
	 char* chavep;
	 if(ptaux!=NULL) chavep= ptaux->chave;
	 while (ptaux !=NULL && strcmp(chavep,Chave)!=0)
	{
		ptaux=ptaux->prox;
		if(ptaux!=NULL) chavep = ptaux->chave;
	}

     /*verifica se achou*/

    if(ptaux!=NULL && strcmp(ptaux->chave,Chave)==0)
		{ //ptaux->prox =NULL;
		return dicionario_CriaCopiaUnica(ptaux);}
    else
		return NULL;

}

/**
 * Retorna Tipo dicionario com o tamanho do vetor alterado
 * name: dicionario_fatorVetor
 * @param fator: Fator inferente do novo tamanho do vetor
 * @param dicio: Tipo dicionário que terá o tamanho do vetor alterado
 * @return Tipo dicionario atualizado
 *
 */
comp_dict_t* dicionario_fatorVetor(tipo_vetor* fator, comp_dict_t* dicio)
{
	if(dicio!=NULL)
	{
		dicio->tamanho = dicio->tamanho*fator->tamanho;
		dicio->vetor = fator;
	}
	return dicio;
}

/**
 * (Re)Define dimensão do vetor e o tamanho
 * name: dicionario_novaDimensao
 * @param fator: Fator que influencia no tamanho do vetor
 * @param anterior: Vetor que terá seus valores alterados
 * @return Vetor alterado
 *
 */
tipo_vetor* dicionario_novaDimensao(int fator, tipo_vetor* anterior)
{
	if(anterior==NULL)
	{
		tipo_vetor* novo = (tipo_vetor*) malloc(sizeof(tipo_vetor));
		novo->dimensao = 1;
		novo->tamanho = fator;
		
		novo->dimensoes = (tam_dimensao*) malloc(sizeof(tam_dimensao));
		novo->dimensoes->tamanho = fator;
		novo->dimensoes->prox = NULL;

		return novo;
	}
	else
	{
		anterior->dimensao++;
		anterior->tamanho = anterior->tamanho*fator;
		
		tam_dimensao* aux = anterior->dimensoes;
		while(aux->prox!=NULL)
			aux=aux->prox;
			
		tam_dimensao* novo = (tam_dimensao*) malloc(sizeof(tam_dimensao));
		novo->prox = anterior->dimensoes;
		novo->tamanho = fator;
		
		anterior->dimensoes = novo;
		
		return anterior;
	}
}


//contagem começa com 1 
int dicionario_elementosDimensaoK(int k,comp_dict_t* vetor)
{
	
	tam_dimensao* aux = vetor->vetor->dimensoes;
	
	int i=1;
	while(i<k)
	{
		aux = aux->prox;
		i++;
	}
	
	return aux->tamanho;
}

/**
 * Mostra o dicionário na tela
 * name: dicionario_imprime
 * @param dicio: Dicionário a ser impresso na tela
 * @return
 *
 */
void dicionario_imprime(comp_dict_t* dicio)
{
	comp_dict_t* aux = dicio;
	printf("IMPRIMINDO DICIONARIO\n");
	while(aux!=NULL)
	{
		printf("\nChave: %s\n", aux->chave);
		printf("Linha: %d\n", aux->linha);
    		char* tipo = (char*) IKS_TIPO_para_texto(aux->tipo);
		printf("Tipo: %s\n",tipo);
		printf("Tamanho: %d\n", aux->tamanho);
		printf("Deslocamento: %d\n", aux->deslocamento);
		printf("Tipo id: %s\n", TIPO_ID_para_texto(aux->tipo_id));
		if(aux->tipo_id == TIPO_ID_FUNCAO)
		{
			printf("Retorno funcao: %d\n", aux->funcao->tipo_retorno);
			printf("Parametros funcao: %d\n", aux->funcao->n_param);
			int i;
			tipo_param* auxtp = aux->funcao->param;
			for(i=0; i<aux->funcao->n_param;i++)
			{
				printf("Param %d tipo %d\n",i,auxtp->tipo);
				auxtp=auxtp->prox;
			}
		}
		else if(aux->tipo_id == TIPO_ID_VETOR)
		{
			printf("Dimensoes vetor: %d\n",aux->vetor->dimensao);
			tam_dimensao* auxdim = aux->vetor->dimensoes;
			int i=0;
			while(auxdim!=NULL)
			{
				printf("Dimensao %i com %d elementos\n",i,auxdim->tamanho);
				auxdim=auxdim->prox;
				i++;
			}
		}

		aux = aux->prox;
	}
	printf("\nFIM DICIONARIO\n\n");
}
