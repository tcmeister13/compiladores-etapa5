/*
   main.c

   Arquivo principal do analisador sintático.
*/
#include "main.h"
extern int linha;
comp_tree_t* arvore;
escopo_tree_t* tabela_simbolos_escopo;

void yyerror (char const *mensagem)
{
  fprintf (stderr, "%s na linha %d\n", mensagem, linha);
}

int main (int argc, char **argv)
{
	gv_init("saida.dot");
	tabela_simbolos_escopo = escopo_criaEscopoGlobal(); 
	arvore = NULL;
	
	//printf("##################################\nPROGRAMA:\n##################################\n\n\n");
	int resultado = yyparse();
	//escopo_imprime(tabela_simbolos_escopo);
	//printf("\n\nSYNTAX TREE\n\n");
	
	
	
	//comp_list_t* l = TAC_geraCodigo(arvore);
	
	//printf("\n##################################\nPROGRAMA ILOC GERADO:\n##################################\n\n");
	//TAC_imprime(l);
	//printf("\n##################################\nARVORE GERADA:\n##################################\n");
	//arvore_imprime(arvore);
	TAC_imprime(arvore->TACs);
	//printf("\n\nESCOPOS\n\n");
	//escopo_imprime(tabela_simbolos_escopo);
	declara_gv(arvore);
	
	
	gv_close();
	
	//printf("retorno:%d\n",resultado);
	
		
	return resultado;
	
	
}

