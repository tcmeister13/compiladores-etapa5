/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stringf.h"

/**
 * Aloca e já copia uma string
 * name: string_copia
 * @param de: string a ser copiada
 * @return copia da string passada
 *
 */
char* string_copia(char* de)
{
	char* copia = de;
	int tamanho = 0;
	
	int i =0;
	while(de[i]!='\0')
	{
		tamanho ++, i++;
	}
	char* para = malloc(strlen(de)+1);	
	//para = "";
	
	strcpy(para,de);
	
	//for(i=0;i<tamanho;i++)
		//para[i]=de[i];	
	
	return para;
}

/**
 * Aloca e já copia uma string sem o primeiro e ultimo elementos
 * name: string_copia_sem_primeiro_e_ultimo
 * @param de: string a ser copiada
 * @return copia da string passada sem primeiro e ultimo elementos
 *
 */
char* string_copia_sem_primeiro_e_ultimo(char* de)
{
	char* para = malloc(strlen(de));
	strcpy(para,de+1);
	
	para[strlen(de)-2] = '\0';
		
	//printf("#%s#\n",para);
	
	
	return para;
}
